<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\cms\common\models\ContentCategory */

$this->title                   = Yii::t('taxonomy', 'Creating Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('taxonomy', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
