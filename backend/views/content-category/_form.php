<?php

use emilasp\cms\common\models\ContentCategory;
use emilasp\core\extensions\ImeraviEditor\ImperaviEditor;
use emilasp\media\models\File;
use emilasp\seo\widgets\SeoForm\SeoForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\cms\common\models\ContentCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?php if ($model->image) : ?>
        <?= Html::a(
            Html::img($model->image->getUrl(File::SIZE_ICO), [
                'class'    => 'img-thumbnail media-object',
                'data-src' => $model->image->getUrl(File::SIZE_MAX),
                'alt'      => $model->image->title,
            ]),
            $model->image->getUrl(File::SIZE_MAX),
            [
                'data-jbox-image' => 'gl',
                'data-pjax'       => 0,
            ]
        ) ?>
    <?php else : ?>
        <?= Html::img(File::getNoImageUrl(File::SIZE_ICO), ['class' => 'img-thumbnail media-object']); ?>
    <?php endif ?>

    <?= $form->field($model, 'imageUpload')->fileInput()->label(false) ?>

    <hr/>


    <div class="row">
        <div class="col-md-2">
            <ul class="nav nav-pills flex-column">
                <li>
                    <a data-toggle="tab" href="#base" class="nav-link active">
                        <?= Yii::t('site', 'Tab Base') ?>
                    </a>
                </li>
                <li><a data-toggle="tab" href="#seo" class="nav-link"><?= Yii::t('site', 'Tab Seo') ?></a></li>
            </ul>
        </div>

        <div class="col-md-10">
            <div class="tab-content">
                <div id="base" class="tab-pane active">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'text')->widget(ImperaviEditor::class, [
                                'settings' => [
                                    'imageUpload'       => Url::to(['/cms/content-category/image-upload']),
                                    'imageManagerJson'  => Url::to([
                                        '/cms/article/images-get',
                                        'id'     => $model->id,
                                        'object' => $model::className()
                                    ]),
                                    'uploadImageFields' => ['id' => $model->id, 'object' => $model::className()],
                                ],
                            ]); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'short_text')->widget(ImperaviEditor::class, [
                                'settings' => [
                                    'imageUpload'       => Url::to(['/cms/content-category/image-upload']),
                                    'imageManagerJson'  => Url::to([
                                        '/cms/article/images-get',
                                        'id'     => $model->id,
                                        'object' => $model::className()
                                    ]),
                                    'uploadImageFields' => ['id' => $model->id, 'object' => $model::className()],
                                ],
                            ]); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'status')->dropDownList(ContentCategory::$statuses) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'type')->dropDownList(ContentCategory::$types) ?>
                        </div>
                    </div>

                </div>
                <div id="seo" class="tab-pane fade">
                    <?= SeoForm::widget(['form' => $form, 'model' => $model]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="text-right">
        <?php if (!$model->isNewRecord) : ?>
            <?= Html::button(Yii::t('site', 'Delete'), [
                'id'      => 'category-delete',
                'data-id' => $model->id,
                'class'   => 'btn btn-danger'
            ]) ?>
        <?php endif; ?>

        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => 'btn btn-success']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
