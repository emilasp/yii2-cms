<?php

use emilasp\cms\common\models\ContentCategory;
use emilasp\core\components\gridview\RelationColumn;
use emilasp\media\components\gridview\ImageColumn;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\cms\common\models\search\ContentCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('taxonomy', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Html::a(Yii::t('taxonomy', 'Create Category'), ['create'], ['class' => 'btn btn-success']) ?></p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'mainImage',
                'class'     => ImageColumn::className(),
            ],
            [
                'attribute' => 'id',
                'class'     => '\kartik\grid\DataColumn',
                'width'     => '100px',
                'hAlign'    => GridView::ALIGN_CENTER,
                'vAlign'    => GridView::ALIGN_MIDDLE,
            ],
            [
                'attribute' => 'name',
                'class'     => RelationColumn::className(),
                'name'      => 'name',
                'route'     => '/cms/content-category/update',
            ],
            'type',
            [
                'attribute' => 'status',
                'value'     => function ($model, $key, $index, $column) {
                    return ContentCategory::$statuses[$model->status];
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                'filter'    => ContentCategory::$statuses,
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
