<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\cms\common\models\ContentTag */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('taxonomy', 'Tag'),
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('taxonomy', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="content-tag-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
