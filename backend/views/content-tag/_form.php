<?php

use emilasp\cms\common\models\ContentTag;
use emilasp\seo\widgets\SeoForm\SeoForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\cms\common\models\ContentTag */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-tag-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <ul class="nav nav-pills flex-column">
                <li>
                    <a data-toggle="tab" href="#base" class="nav-link active">
                        <?= Yii::t('site', 'Tab Base') ?>
                    </a>
                </li>
                <li><a data-toggle="tab" href="#seo" class="nav-link"><?= Yii::t('site', 'Tab Seo') ?></a></li>
            </ul>
        </div>

        <div class="col-md-9">
            <div class="tab-content">
                <div id="base" class="tab-pane active">
                    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'frequency')->textInput() ?>

                    <?= $form->field($model, 'status')->dropDownList(ContentTag::$statuses) ?>

                </div>
                <div id="seo" class="tab-pane fade">
                    <?= SeoForm::widget(['form' => $form, 'model' => $model]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="text-right">
        <?php if (!$model->isNewRecord) : ?>
            <?= Html::button(Yii::t('site', 'Delete'), [
                'id'      => 'category-delete',
                'data-id' => $model->id,
                'class'   => 'btn btn-danger'
            ]) ?>
        <?php endif; ?>

        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => 'btn btn-success']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
