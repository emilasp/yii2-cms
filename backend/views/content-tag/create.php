<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\cms\common\models\ContentTag */

$this->title = Yii::t('taxonomy', 'Creating Tag');
$this->params['breadcrumbs'][] = ['label' => Yii::t('taxonomy', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-tag-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
