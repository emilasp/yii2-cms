<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\cms\common\models\search\ContentTagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('taxonomy', 'Tags');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-tag-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Html::a(Yii::t('taxonomy', 'Create Tag'), ['create'], ['class' => 'btn btn-success']) ?></p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'slug',
            'name',
            'frequency',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
