<?php

use dosamigos\ckeditor\CKEditor;
use emilasp\cms\common\models\Article;
use emilasp\cms\common\models\ContentTask;
use emilasp\json\models\DynamicModel;
use emilasp\json\widgets\DynamicFields\DynamicFields;

?>

<div id="base" class="tab-pane active">
    <hr/>

    <div class="row">
        <div class="col-md-6">

            <?= $form->field($content, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'mission')->widget(CKEditor::className(), [
                'options'       => ['rows' => 3],
                'preset'        => 'basic',
                'clientOptions' => [
                    'filebrowserUploadUrl' => '/media/file/cke-upload'
                ]
            ]) ?>

            <?= $form->field($model, 'plan')->widget(CKEditor::className(), [
                'options'       => ['rows' => 3],
                'preset'        => 'basic',
                'clientOptions' => [
                    'filebrowserUploadUrl' => '/media/file/cke-upload'
                ]
            ]) ?>


            <?= DynamicFields::widget([
                'form'        => $form,
                'model'       => $model,
                'attribute'   => 'checklist',
                'addEmptyRow' => false,
                'scheme'      => DynamicModel::SCHEME_CUSTOM,
                'viewFile'    => '@vendor/emilasp/yii2-cms/backend/views/content-task/json/_checklist',
            ]); ?>


        </div>
        <div class="col-md-6">

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'type')->dropDownList($model::$types) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'status')->dropDownList($model::$statuses) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'type_content')->dropDownList(ContentTask::$contentTypes) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'content_id')->textInput() ?>
                </div>
            </div>

            <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'keyword')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'keywords')->textarea(['rows' => 3]) ?>

            <?= $form->field($model, 'length')->input('number') ?>

        </div>
    </div>

</div>
