<?php

use emilasp\cms\common\models\Article;
use emilasp\cms\common\models\ContentCategory;
use emilasp\cms\common\models\ContentTask;
use emilasp\taxonomy\extensions\CategorySelectTree\CategorySelectTree;
use emilasp\users\common\models\User;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\cms\common\models\search\ContentTaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cms', 'Content Tasks');

$this->params['breadcrumbs'][] = ['label' => Yii::t('cms', 'Content'), 'url' => ['/cms/content/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-task-index">

    <p><?= Html::a(Yii::t('cms', 'Add Content Task'), ['create'], ['class' => 'btn btn-success']) ?></p>


    <div class="row">
        <div class="col-md-3">
            <?= CategorySelectTree::widget([
                'id'            => 'index-categories',
                'rootId'            => Yii::$app->controller->module->getSetting('categoryRootId'),
                'model'             => new Article(),
                'categoryClassName' => ContentCategory::className(),
                'attribute'         => 'categoriesId',
                'options' => [
                    'selectMode' => 1,
                    'select' => '
                        function(event, data) {
                                var selNodes = data.tree.getSelectedNodes();
                                var selKeys = $.map(selNodes, function(node){return node.key;});
                                var category_id = "";
                                if ($.isArray(selKeys) && selKeys.length) {
                                    $("#{{selectorFiled}}-input").val("[" + selKeys.join(", ") + "]");                                     
                                   
                                    $("#{{selectorFiled}}-input").change()
                                }
                            }
                    '
                ]
            ]) ?>
        </div>
        <div class="col-md-9">
            <?php Pjax::begin(['id' => 'pjax-index-categories-grid']); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'  => $searchModel,
                'columns'      => [
                    ['class' => '\kartik\grid\SerialColumn'],

                    [
                        'attribute' => 'content_id',
                        'value'     => function ($model) {
                            return $model->content->name ?? null;
                        },
                        'class'     => '\kartik\grid\DataColumn',
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        //'width'     => '150px',
                    ],
                    [
                        'attribute' => 'type',
                        'value'     => function ($model, $key, $index, $column) {
                            return ContentTask::$types[$model->type];
                        },
                        'class'     => '\kartik\grid\DataColumn',
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '150px',
                        'filter'    => ContentTask::$types,
                    ],
                    'keyword',
                    [
                        'attribute' => 'type_content',
                        'value'     => function ($model, $key, $index, $column) {
                            return ContentTask::$contentTypes[$model->type_content];
                        },
                        'class'     => '\kartik\grid\DataColumn',
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '150px',
                        'filter'    => ContentTask::$contentTypes,
                    ],
                   /* [
                        'attribute' => 'checklist',
                        'value'     => function ($model, $key, $index, $column) {
                            $check = true;
                            foreach (json_decode($model->checklist, true) as $row) {
                                if (!$row['enabled']) {
                                    $check = false;
                                    break;
                                }
                            }
                            return Html::checkbox('checklist', $check);
                        },
                        'class'     => '\kartik\grid\DataColumn',
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '100px',
                        'filter'    => ContentTask::$statuses,
                        'format'    => 'raw'
                    ],*/
                    [
                        'attribute' => 'status',
                        'value'     => function ($model, $key, $index, $column) {
                            return ContentTask::$statuses[$model->status];
                        },
                        'class'     => '\kartik\grid\DataColumn',
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '150px',
                        'filter'    => ContentTask::$statuses,
                    ],
                    'created_at:datetime',

                   /* [
                        'attribute'           => 'created_by',
                        'value'               => function ($model) {
                            return $model->createdBy->username ?? null;
                        },
                        'class'               => '\kartik\grid\DataColumn',
                        'hAlign'              => GridView::ALIGN_LEFT,
                        'vAlign'              => GridView::ALIGN_MIDDLE,
                        'width'               => '150px',
                        'filterType'          => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'language'      => \Yii::$app->language,
                            'data'          => User::find()->map('id', 'username')->all(),
                            'options'       => ['placeholder' => '-выбрать-'],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ],
                    ],*/

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>

            <?php Pjax::end(); ?>
        </div>
    </div>



</div>

<?php
$js = <<<JS
    $(document).on('change', '#index-categories-input', function() {
        var categories = $(this).val();
       
        $.pjax({
               container:"#pjax-index-categories-grid",
               method:'GET',
               timeout: 0,
               data:{categories:categories},
               scrollTo:false
           });
    });
JS;

$this->registerJs($js);
