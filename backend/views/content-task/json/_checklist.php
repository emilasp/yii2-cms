
<div class="item panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title "><?= Yii::t('json', 'Checklist') ?></h3>
    </div>
    <div class="panel-body">

        <?php foreach ($models as $i => $model) : ?>
            <div class="row row-item">
                <div class="col-sm-1">
                    <?= $form->field($model, "[{$i}]enabled")->checkbox(['label' => false])->label(false) ?>
                </div>

                <div class="col-sm-5">
                    <label class="text-primary"><?= $model->name ?></label>
                    <?= $form->field($model, "[{$i}]name")->hiddenInput()->label(false) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, "[{$i}]description")
                        ->textInput(['maxlength' => true, 'placeholder' => 'description'])->label(false) ?>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>