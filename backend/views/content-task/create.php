<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\cms\common\models\ContentTask */

$this->title                   = Yii::t('cms', 'Create Content Task');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cms', 'Content'), 'url' => ['/cms/content/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('cms', 'Content Tasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-task-create">

    <?= $this->render('_form', [
        'model'   => $model,
        'content' => $content,
    ]) ?>

</div>
