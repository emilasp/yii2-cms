<?php

use dosamigos\ckeditor\CKEditor;
use emilasp\cms\common\models\Content;
use emilasp\cms\common\models\ContentTask;
use emilasp\json\models\DynamicModel;
use emilasp\json\widgets\DynamicFields\DynamicFields;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\cms\common\models\ContentTask */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="content-task-form">

        <?php $form = ActiveForm::begin(['options' => ['id' => 'content-task-form-id']]); ?>

        <?= $form->errorSummary($model); ?>

        <div class="row">
            <div class="col-md-2">
                <ul class="nav nav-pills flex-column">
                    <li>
                        <a data-toggle="tab" href="#base" class="nav-link active"><?= Yii::t('site', 'Tab Base') ?></a>
                    </li>
                    <li role="presentation">
                        <a aria-controls="taxonomy" role="tab" data-toggle="tab" href="#taxonomy" class="nav-link">
                            <?= Yii::t('taxonomy', 'Taxonomies') ?>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-md-10">
                <div class="tab-content">
                    <?= $this->render('tabs/_base', ['form' => $form, 'model' => $model, 'content' => $content]) ?>
                    <?= $this->render('tabs/_taxonomy', ['form' => $form, 'model' => $content]) ?>
                </div>
            </div>
        </div>

        <div class="form-group text-right">

            <?php if (!$model->isNewRecord) : ?>
                <?= Html::button(
                    $model->content_id ? Yii::t('cms', 'To Content') : Yii::t('cms', 'Create Content'),
                    ['class' => 'btn btn-primary btn-create-content']
                ) ?>
            <?php endif; ?>

            <?= Html::submitButton(Yii::t('site', 'Save'), ['class' => 'btn btn-success']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$urlToCreateContent = Url::current(['create' => 1]);

$js = <<<JS
    $('.btn-create-content').on('click', function() {
        var form = $('#content-task-form-id');
        
        console.log(form);
        
        form.attr('action', '{$urlToCreateContent}');
        form.submit();
    })
JS;

$this->registerJs($js);