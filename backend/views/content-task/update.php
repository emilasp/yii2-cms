<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\cms\common\models\ContentTask */

$this->title = Yii::t('cms', 'Update Content Task');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cms', 'Content'), 'url' => ['/cms/content/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('cms', 'Content Tasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="content-task-update">

    <?= $this->render('_form', [
        'model'   => $model,
        'content' => $content,
    ]) ?>

</div>
