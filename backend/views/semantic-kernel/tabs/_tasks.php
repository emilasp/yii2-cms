<?php

use dosamigos\selectize\SelectizeTextInput;
use emilasp\cms\common\models\ContentCategory;
use emilasp\taxonomy\extensions\CategorySelectTree\CategorySelectTree;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

?>

    <div id="tasks" class="tab-pane fade">

        <h2 data-toggle="collapse" href="#add-key-form" role="button" aria-expanded="false"
            aria-controls="add-key-form" class="cursor-pointer">
            Добавление ключа
        </h2>

        <div class="row collapse" id="add-key-form">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-md-6">
                        <?= Html::textInput('create-task-form-title', null, [
                            'class'       => 'create-task-form-title form-control',
                            'placeholder' => 'Title',
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= Html::textInput('create-task-form-url', null, [
                            'class'       => 'create-task-form-url form-control',
                            'placeholder' => 'Url на статью сайта донора',
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-10">
                        <?= Html::textInput('create-task-form-keyword', null, [
                            'class'       => 'create-task-form-keyword form-control',
                            'placeholder' => 'Главный ключ',
                        ]) ?>
                    </div>
                    <div class="col-md-2">
                        <?= Html::textInput('create-task-form-length', null, [
                            'class'       => 'create-task-form-length form-control',
                            'placeholder' => 'Длина статьи',
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <?= Html::textarea('create-task-form-keyword', null, [
                            'class'       => 'create-task-form-keywords form-control',
                            'placeholder' => 'Дополнительные ключи',
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= Html::textarea('create-task-form-mission', null, [
                            'class'       => 'create-task-form-mission form-control',
                            'placeholder' => 'Миссия',
                        ]) ?>
                    </div>
                </div>

                <div class="text-right">
                    <?= Html::button(
                        Html::tag('i', '', ['class' => 'fa fa-plus']) . ' Добавить',
                        ['class' => 'btn btn-primary btn-flat btn-add-keyword']
                    ) ?>
                </div>
            </div>
        </div>


        <h2>Список ключей в ядре</h2>

        <?php Pjax::begin(['id' => 'pjax-grid-tasks']); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProviderTasks,
            'filterModel'  => $searchModelTasks,
            'columns'      => [
                ['class' => '\kartik\grid\SerialColumn'],

                'keyword',
                //'keywords',
                'url',
                'length',

                [
                    'class'    => '\kartik\grid\ActionColumn',
                    'template' => '{update} {delete}',

                    'urlCreator' => function ($action, $model, $key, $index) {
                        return ['/cms/content-task/' . $action, 'id' => $model->id, 'hvost' => time()];
                    },
                ]
            ]
        ]); ?>

        <?php Pjax::end(); ?>


    </div>

<?php
$url = Url::toRoute(['/cms/semantic-kernel/create-task']);

$js = <<<JS

$(document).on('click', '.btn-add-keyword', function () {
    var title = $('.create-task-form-title').val();
    var keyword = $('.create-task-form-keyword').val();
    var keywords = $('.create-task-form-keywords').val();
    var url = $('.create-task-form-url').val();
    var length = $('.create-task-form-length').val();
    var mission = $('.create-task-form-mission').val();
    var kernelId = {$model->id};
    
    $.ajax({
        type: 'POST',
        url: '{$url}',
        dataType: "json",
        data: 'kernelId=' + kernelId + '&title=' + title + '&keyword=' + keyword + '&keywords='+keywords
         + '&url='+url + '&length='+length + '&mission='+mission,
        success: function(msg) {
            notice(msg['message'], (msg['status']=='1' ? 'green' : 'red'));
            
             $.pjax({
                 container:"#pjax-grid-tasks",
                 timeout: 0,
                 push:false,
                 scrollTo:false
             });
             
             $('.create-task-form-title').val('');
             $('.create-task-form-keyword').val('');
             $('.create-task-form-keywords').val('');
             $('.create-task-form-url').val('');
             $('.create-task-form-length').val('');
             $('.create-task-form-mission').val('');
        }
    });
});
JS;

$this->registerJs($js);