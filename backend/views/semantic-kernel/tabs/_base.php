<?php
use emilasp\cms\common\models\CmsSemanticKernel;
?>

<div id="base" class="tab-pane active">
    <hr/>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList(CmsSemanticKernel::$statuses) ?>

</div>
