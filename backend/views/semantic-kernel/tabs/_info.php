<?php

use dosamigos\selectize\SelectizeTextInput;
use emilasp\cms\common\models\ContentCategory;
use emilasp\taxonomy\extensions\CategorySelectTree\CategorySelectTree;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

?>
    <div id="info" class="tab-pane fade">
        <h2>Выписываем ядро с сайтов конкурентов</h2>

        <ul>
            <li>Открываем сайт конкурента</li>
            <li>Если есть, то sitemap.xml</li>
            <li>Ищем нужную рубрику</li>
            <li>Выписываем каждую статью</li>
        </ul>

        <h2>Выписываем ключ с сайта конкурента</h2>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Шаг</th>
                <th>Описание</th>
            </tr>
            </thead>
            <tbody>
            <tr><th>1</th><td>Главный ключ берём из Title(не из h1)</td></tr>
            <tr><th>2</th><td>Вписываем главный ключ в поиск Яндекс и уточняем его актуальную формулировку</td></tr>
            <tr><th>3</th><td>Выписываем все доп слова из подсказок Яндекса(хвосты)</td></tr>
            <tr><th>4</th><td>Wordstat - частотность + 2-3 доп ключа. "Синонимы" главного ключа.</td></tr>
            <tr><th>5</th><td>Размер статьи. Смотрим статьи по ключу в ТОП 2 Яндекса.</td></tr>
            </tbody>
        </table>

    </div>
