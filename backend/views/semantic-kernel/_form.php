<?php

use emilasp\cms\common\models\CmsSemanticKernel;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\cms\common\models\CmsSemanticKernel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cms-semantic-kernel-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">

        <?= $form->errorSummary($model); ?>

        <div class="row">
            <div class="col-md-2">
                <ul class="nav nav-pills flex-column">
                    <li>
                        <a data-toggle="tab" href="#base" class="nav-link active"><?= Yii::t('site', 'Tab Base') ?></a>
                    </li>

                    <?php if (!$model->isNewRecord) : ?>
                        <li role="presentation">
                            <a role="tab" data-toggle="tab" href="#tasks" class="nav-link">
                                <?= Yii::t('cms', 'Tasks') ?>
                            </a>
                        </li>
                    <?php endif; ?>

                    <li role="presentation">
                        <a role="tab" data-toggle="tab" href="#info" class="nav-link">
                            <?= Yii::t('cms', 'Info') ?>
                        </a>
                    </li>

                </ul>
            </div>

            <div class="col-md-10">
                <div class="tab-content">
                    <?= $this->render('tabs/_base', ['form' => $form, 'model' => $model]) ?>
                    <?= $this->render('tabs/_info', ['form' => $form, 'model' => $model]) ?>

                    <?php if (!$model->isNewRecord) : ?>
                    <?= $this->render('tabs/_tasks', [
                        'form'              => $form,
                        'model'             => $model,
                        'dataProviderTasks' => $dataProviderTasks ?? null,
                        'searchModelTasks'  => $searchModelTasks ?? null,
                    ]) ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>
    <div class="box-footer text-right">
        <?= Html::submitButton(
            Html::tag('i', '', ['class' => 'fa fa-floppy-o']) . ' '
            . ($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Save')),
            ['class' => 'btn btn-success btn-flat']
        ) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
