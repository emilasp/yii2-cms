<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\cms\common\models\CmsSemanticKernel */

$this->title = Yii::t('site', 'Created');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cms', 'Cms Semantic Kernels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cms-semantic-kernel-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
