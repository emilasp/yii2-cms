<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\cms\common\models\CmsSemanticKernel */

$this->title                   = Yii::t('site', 'Update {modelClass}: ', [
        'modelClass' => Yii::t('cms', 'Cms Semantic Kernel'),
    ]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cms', 'Cms Semantic Kernels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Updated');
?>
<div class="cms-semantic-kernel-update">

    <?= $this->render('_form', [
        'model'             => $model,
        'dataProviderTasks' => $dataProviderTasks,
        'searchModelTasks'  => $searchModelTasks,
    ]) ?>

</div>
