<?php

use emilasp\cms\common\models\CmsSemanticKernel;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel emilasp\cms\common\models\search\CmsSemanticKernelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cms', 'Cms Semantic Kernels');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cms-semantic-kernel-index box box-primary">

    <div class="box-header with-border text-right">
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-plus']) . ' ' . Yii::t('site', 'Create'), ['create'], [
            'class' => 'btn btn-success btn-flat'
        ]) ?>
    </div>

    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                ['class' => '\kartik\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'class'     => '\kartik\grid\DataColumn',
                    'width'     => '100px',
                    'hAlign'    => GridView::ALIGN_CENTER,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                ],
                'name',
                'description:ntext',
                [
                    'attribute' => 'status',
                    'value'     => function ($model, $key, $index, $column) {
                        return CmsSemanticKernel::$statuses[$model->status];
                    },
                    'class'     => '\kartik\grid\DataColumn',
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'width'     => '150px',
                    'filter'    => CmsSemanticKernel::$statuses,
                ],
                'created_at',
                // 'updated_at',
                // 'created_by',
                // 'updated_by',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

            <?php Pjax::end(); ?>

    </div>

</div>
