<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\cms\common\models\News */

$this->title                   = Yii::t('cms', 'Creating News');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cms', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-create">

    <?= $this->render('_form', [
        'model'     => $model,
        'modelTask' => $modelTask,
    ]) ?>

</div>
