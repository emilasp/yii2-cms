<?php

use emilasp\media\extensions\FileInputWidget\FileInputWidget;
?>

<div id="images" class="tab-pane fade">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('media', 'Files') ?></h3>
        </div>
        <div class="panel-body">

            <?php if ($model->image) : ?>
                <a href="<?= $model->image->getUrl('max') ?>"
                   data-jbox-image="gallery<?= $model->image->id ?>">
                    <img src="<?= $model->image->getUrl('ico') ?>"
                         alt="<?= $model->image->title ?>" class="img-thumbnail">
                </a>
            <?php endif; ?>

            <?= $form->field($model, 'imageUpload')->fileInput() ?>

            <?= FileInputWidget::widget([
                'model'         => $model,
                'type'          => FileInputWidget::TYPE_MULTI,
                'title'         => true,
                'description'   => true,
                'showTitle'     => true,
                'previewHeight' => 20,
            ]) ?>
        </div>
    </div>

</div>