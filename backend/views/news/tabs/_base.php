<?php

use dosamigos\ckeditor\CKEditor;
use dosamigos\selectize\SelectizeTextInput;
use emilasp\cms\common\models\Article;

?>

<div id="base" class="tab-pane active">
    <hr/>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'formTags')->widget(SelectizeTextInput::className(), [
        'loadUrl'       => ['/taxonomy/tag/search'],
        'options'       => ['class' => 'form-control'],
        'clientOptions' => [
            'plugins'     => ['remove_button', 'restore_on_backspace'],//, 'drag_drop'
            'valueField'  => 'name',
            'labelField'  => 'name',
            'searchField' => ['name'],
            'create'      => true,
        ],
    ])->label('Теги')->hint('Используйте запятые для разделения меток') ?>

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'options'       => ['rows' => 8],
        'preset'        => 'standart',
        'clientOptions' => [
            'filebrowserUploadUrl' => '/media/file/cke-upload'
        ]
    ]) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList(Article::$statuses) ?>
        </div>
    </div>

</div>
