<?php
use emilasp\seo\widgets\SeoForm\SeoForm;
?>


<div id="seo" class="tab-pane fade">
    <?= SeoForm::widget(['form' => $form, 'model' => $model]) ?>
</div>