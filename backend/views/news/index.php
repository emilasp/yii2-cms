<?php

use emilasp\cms\common\models\News;
use emilasp\media\components\gridview\ImageColumn;
use emilasp\users\common\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\cms\common\models\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cms', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-index">

    <?php Pjax::begin(); ?>

    <p>
        <?= Html::a(Yii::t('cms', 'Create News'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => '\kartik\grid\SerialColumn'],
            [
                'attribute' => 'image',
                'class'     => ImageColumn::className(),
            ],
            [
                'attribute' => 'id',
                'class'     => '\kartik\grid\DataColumn',
                'width'     => '100px',
                'hAlign'    => GridView::ALIGN_CENTER,
                'vAlign'    => GridView::ALIGN_MIDDLE,
            ],

            /*[
                'attribute' => 'category',
                'value'     => function ($model, $key, $index, $column) {
                    return $model->category->name;
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                //'filter'    => Article::$statuses,
            ],*/
            'name',
            //'entry:ntext',
            //'content:ntext',
            // 'conclusion:ntext',
            [
                'attribute' => 'status',
                'value'     => function ($model, $key, $index, $column) {
                    return News::$statuses[$model->status];
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                'filter'    => News::$statuses,
            ],
            'created_at:datetime',

            [
                'attribute'           => 'created_by',
                'value'               => function ($model) {
                    return $model->createdBy->username;
                },
                'class'               => '\kartik\grid\DataColumn',
                'hAlign'              => GridView::ALIGN_LEFT,
                'vAlign'              => GridView::ALIGN_MIDDLE,
                'width'               => '150px',
                'filterType'          => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'language'      => \Yii::$app->language,
                    'data'          => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                    'options'       => ['placeholder' => '-выбрать-'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
