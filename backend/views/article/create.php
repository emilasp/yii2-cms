<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\cms\common\models\Article */

$this->title                   = Yii::t('cms', 'Creating Article');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cms', 'Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-create">

    <?= $this->render('_form', [
        'model'     => $model,
        'modelTask' => $modelTask,
    ]) ?>

</div>
