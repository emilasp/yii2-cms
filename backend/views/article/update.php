<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\cms\common\models\Article */

$this->title                   = Yii::t('site', 'Updated') . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cms', 'Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="content-update">

    <?= $this->render('_form', [
        'model'     => $model,
        'modelTask' => $modelTask,
    ]) ?>

</div>
