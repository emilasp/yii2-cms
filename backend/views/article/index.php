<?php

use emilasp\cms\common\models\Article;
use emilasp\core\components\gridview\RelationColumn;
use emilasp\media\components\gridview\ImageColumn;
use emilasp\users\common\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\cms\common\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cms', 'Articles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-index">

    <?php Pjax::begin(); ?>

    <p>
        <?= Html::a(Yii::t('cms', 'Create Article'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => '\kartik\grid\SerialColumn'],
            [
                'attribute' => 'mainImage',
                'class'     => ImageColumn::className(),
            ],
            [
                'attribute' => 'id',
                'class'     => '\kartik\grid\DataColumn',
                'width'     => '100px',
                'hAlign'    => GridView::ALIGN_CENTER,
                'vAlign'    => GridView::ALIGN_MIDDLE,
            ],

            /*[
                'attribute' => 'category',
                'value'     => function ($model, $key, $index, $column) {
                    return $model->category->name;
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                //'filter'    => Article::$statuses,
            ],*/
            [
                'attribute' => 'name',
                'class'     => RelationColumn::className(),
                'name'      => 'name',
                'route'     => '/cms/article/update',
            ],
            //'entry:ntext',
            //'content:ntext',
            // 'conclusion:ntext',
            [
                'attribute' => 'status',
                'value'     => function ($model, $key, $index, $column) {
                    return Article::$statuses[$model->status];
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                'filter'    => Article::$statuses,
            ],
            'created_at:datetime',

            [
                'attribute'           => 'created_by',
                'value'               => function ($model) {
                    return $model->createdBy->username;
                },
                'class'               => '\kartik\grid\DataColumn',
                'hAlign'              => GridView::ALIGN_LEFT,
                'vAlign'              => GridView::ALIGN_MIDDLE,
                'width'               => '150px',
                'filterType'          => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'language'      => \Yii::$app->language,
                    'data'          => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                    'options'       => ['placeholder' => '-выбрать-'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
