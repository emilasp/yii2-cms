<?php

use dosamigos\ckeditor\CKEditor;
use emilasp\json\models\DynamicModel;
use emilasp\json\widgets\DynamicFields\DynamicFields;

?>

<div id="task" class="tab-pane fade">

    <hr/>

    <div class="row">
        <div class="col-md-6">

            <?= $form->field($modelTask, 'mission')->widget(CKEditor::className(), [
                'options'       => ['rows' => 3],
                'preset'        => 'basic',
                'clientOptions' => [
                    'filebrowserUploadUrl' => '/media/file/cke-upload'
                ]
            ]) ?>

            <?= $form->field($modelTask, 'plan')->widget(CKEditor::className(), [
                'options'       => ['rows' => 6],
                'preset'        => 'standard',
                'clientOptions' => [
                    'filebrowserUploadUrl' => '/media/file/cke-upload'
                ]
            ]) ?>

        </div>
        <div class="col-md-6">

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($modelTask, 'type')->dropDownList($modelTask::$types) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($modelTask, 'status')->dropDownList($modelTask::$statuses) ?>
                </div>
            </div>

            <?= $form->field($modelTask, 'url')->textInput(['maxlength' => true]) ?>

            <?= $form->field($modelTask, 'keyword')->textInput(['maxlength' => true]) ?>

            <?= $form->field($modelTask, 'keywords')->textarea(['rows' => 3]) ?>

            <?= $form->field($modelTask, 'length')->input('number') ?>

            <hr />

            <?= DynamicFields::widget([
                'form'        => $form,
                'model'       => $modelTask,
                'attribute'   => 'checklist',
                'addEmptyRow' => false,
                'scheme'      => DynamicModel::SCHEME_CUSTOM,
                'viewFile'    => '@vendor/emilasp/yii2-cms/backend/views/content-task/json/_checklist',
            ]); ?>

        </div>
    </div>

</div>
