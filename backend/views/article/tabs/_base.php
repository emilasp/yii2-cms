<?php

use emilasp\cms\common\models\Article;
use emilasp\cms\backend\widgets\ContentLinking\ContentLinkingWidget;
use emilasp\core\extensions\ImeraviEditor\ImperaviEditor;
use emilasp\core\extensions\ImeraviEditor\plugins\StylingAsset;
use yii\helpers\Url;


//\emilasp\core\extensions\ImeraviEditor\StylingAsset::register($this);



?>

<div id="base" class="tab-pane active">
    <hr/>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'entry')->widget(ImperaviEditor::class, [
        'settings' => [
            'imageUpload'       => Url::to(['/cms/article/image-upload']),
            'imageManagerJson'  => Url::to([
                '/cms/article/images-get',
                'id'     => $model->id,
                'object' => $model::className()
            ]),
            'uploadImageFields' => ['id' => $model->id, 'object' => $model::className()],
        ],
    ]); ?>


    <?= ContentLinkingWidget::widget([]) ?>

    <?= $form->field($model, 'content')->widget(ImperaviEditor::class, [
        'settings' => [
            'imageUpload'       => Url::to(['/cms/article/image-upload']),
            'imageManagerJson'  => Url::to([
                '/cms/article/images-get',
                'id'     => $model->id,
                'object' => $model::className()
            ]),
            'uploadImageFields' => ['id' => $model->id, 'object' => $model::className()],
        ],
    ]); ?>

    <?= $form->field($model, 'conclusion')->widget(ImperaviEditor::class, [
        'settings' => [
            'imageUpload'       => Url::to(['/cms/article/image-upload']),
            'imageManagerJson'  => Url::to([
                '/cms/article/images-get',
                'id'     => $model->id,
                'object' => $model::className()
            ]),
            'uploadImageFields' => ['id' => $model->id, 'object' => $model::className()],
        ],
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList(Article::$statuses) ?>
        </div>
    </div>

</div>
