<?php

use dosamigos\selectize\SelectizeTextInput;
use emilasp\cms\common\models\ContentCategory;
use emilasp\taxonomy\extensions\CategorySelectTree\CategorySelectTree;

?>

<div id="taxonomy" class="tab-pane fade">
    <?= $form->field($model, 'formTags')->widget(SelectizeTextInput::className(), [
        'loadUrl'       => ['/taxonomy/tag/search'],
        'options'       => ['class' => 'form-control'],
        'clientOptions' => [
            'plugins'     => ['remove_button', 'restore_on_backspace'],//, 'drag_drop'
            'valueField'  => 'name',
            'labelField'  => 'name',
            'searchField' => ['name'],
            'create'      => true,
        ],
    ])->label('Теги')->hint('Используйте запятые для разделения меток') ?>

    <?= CategorySelectTree::widget([
        'rootId'            => Yii::$app->controller->module->getSetting('categoryRootId'),
        'model'             => $model,
        'categoryClassName' => ContentCategory::className(),
        'attribute'         => 'categoriesId'
    ]) ?>
</div>
