<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\cms\common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-2">
            <ul class="nav nav-pills flex-column">
                <li>
                    <a data-toggle="tab" href="#base" class="nav-link active"><?= Yii::t('site', 'Tab Base') ?></a>
                </li>
                <li role="presentation">
                    <a aria-controls="taxonomy" role="tab" data-toggle="tab" href="#taxonomy" class="nav-link">
                        <?= Yii::t('taxonomy', 'Taxonomies') ?>
                    </a>
                </li>
                <li role="presentation">
                    <a aria-controls="images" role="tab" data-toggle="tab" href="#images" class="nav-link">
                        <?= Yii::t('media', 'Gallery') ?>
                    </a>
                </li>
                <li role="presentation">
                    <a aria-controls="task" role="tab" data-toggle="tab" href="#task" class="nav-link">
                        <?= Yii::t('cms', 'Content Task') ?>
                    </a>
                </li>
                <li><a data-toggle="tab" href="#seo" class="nav-link"><?= Yii::t('site', 'SEO') ?></a></li>
                <li>
                    <a data-toggle="tab" href="#info" class="nav-link">
                        <?= Yii::t('cms', 'Content task info') ?>
                    </a>
                </li>
            </ul>
        </div>

        <div class="col-md-10">
            <div class="tab-content">
                <?= $this->render('tabs/_base', ['form' => $form, 'model' => $model]) ?>
                <?= $this->render('tabs/_task', ['form' => $form, 'model' => $model, 'modelTask' => $modelTask,]) ?>
                <?= $this->render('tabs/_taxonomy', ['form' => $form, 'model' => $model]) ?>
                <?= $this->render('tabs/_images', ['form' => $form, 'model' => $model]) ?>
                <?= $this->render('tabs/_seo', ['form' => $form, 'model' => $model]) ?>
                <?= $this->render('tabs/_info', ['form' => $form, 'model' => $model]) ?>
            </div>
        </div>
    </div>


    <div class="form-group text-right">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
