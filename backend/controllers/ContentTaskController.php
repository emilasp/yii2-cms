<?php

namespace emilasp\cms\backend\controllers;

use emilasp\cms\common\models\Article;
use emilasp\cms\common\models\Content;
use emilasp\core\components\base\Controller;
use Yii;
use emilasp\cms\common\models\ContentTask;
use emilasp\cms\common\models\search\ContentTaskSearch;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * ContentTaskController implements the CRUD actions for ContentTask model.
 */
class ContentTaskController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ContentTask models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new ContentTaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Dashboard index
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndexDashboard(string $categories = '[]')
    {
        $categories = json_decode($categories);

        $searchModel  = new ContentTaskSearch([
            'categories' => $categories
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-dashboard', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new ContentTask model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(int $create = 0)
    {
        $content = new Article();
        $task    = new ContentTask(['plan' => ContentTask::DEFAULT_PLAN]);

        if ($task->load(Yii::$app->request->post()) && $task->save(false)) {
            if ($task->content->load(Yii::$app->request->post()) && $task->content->save(false)) {
                $task->content->saveTags();

                if ($create) {
                    $route = '/cms/' . ContentTask::$contentTypes[$task->type_content] . '/update';
                    return $this->redirect([$route, 'id' => $task->content_id]);
                }

                return $this->redirect(['update', 'id' => $task->id]);
            }
        }

        return $this->render('create', [
            'model'   => $task,
            'content' => $content,
        ]);
    }

    /**
     * @param     $id
     * @param int $create
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id, $create = 0)
    {
        /** @var ContentTask $task */
        $task    = $this->findModel($id, ContentTask::className());
        $content = $task->content;

        if (!isset($content, $task)) {
            throw new NotFoundHttpException("The user was not found.");
        }

        $content->setTags();

        if ($content->load(Yii::$app->request->post()) && $task->load(Yii::$app->request->post())) {
            if ($task->validate() && $task->save() && $content->validate()) {
                $content->save(false);
                $content->saveTags();

                if ($create) {
                    $route = '/cms/' . ContentTask::$contentTypes[$task->type_content] . '/update';
                    return $this->redirect([$route, 'id' => $task->content_id]);
                }

                return $this->redirect(['index-dashboard', 'id' => $id]);
            }
        }

        return $this->render('update', [
            'model'   => $task,
            'content' => $content,
        ]);
    }

    /**
     * Deletes an existing ContentTask model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, ContentTask::className())->delete();

        return $this->redirect(['index']);
    }
}
