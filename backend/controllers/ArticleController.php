<?php

namespace emilasp\cms\backend\controllers;

use emilasp\cms\common\models\Article;
use emilasp\cms\common\models\ContentTask;
use emilasp\cms\common\models\search\ArticleSearch;
use emilasp\core\components\base\Controller;
use emilasp\core\extensions\ImeraviEditor\actions\ImageManagerAction;
use emilasp\core\extensions\ImeraviEditor\actions\UploadAction;
use emilasp\core\helpers\StringHelper;
use Yii;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ArticleController implements the CRUD actions for Content model.
 */
class ArticleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view', 'create', 'update', 'delete', 'search-linking'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'search-linking'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * @return array
     */
    public function actions()
    {
        return [
            'image-upload' => [
                'class'    => UploadAction::class,
                'url'      => '/uploads/',
                'path'     => '@backend/web/uploads',
                'translit' => true,
            ],

            'images-get' => [
                'class'   => ImageManagerAction::class,
                'url'     => '/uploads/',
                'path'    => '@backend/web/uploads',
                'options' => ['only' => ['*.jpg', '*.jpeg', '*.png', '*.gif', '*.ico']],
            ],
        ];
    }


    /**
     * Lists all Content models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Content model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, Article::className()),
        ]);
    }

    /**
     * Creates a new Content model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $content = new Article();
        $task    = new ContentTask(['plan' => ContentTask::DEFAULT_PLAN]);

        if ($content->load(Yii::$app->request->post()) && $task->load(Yii::$app->request->post())) {
            $isValid = $content->validate();
            $isValid = $task->validate() && $isValid;

            if ($isValid && $content->save(false)) {
                $content->saveTags();
                $task->type_content = ContentTask::CONTENT_TYPE_ARTICLE;
                $task->content_id   = $content->id;
                $task->save(false);
                return $this->redirect(['index', 'id' => $content->id]);
            }
        }

        return $this->render('create', [
            'model'     => $content,
            'modelTask' => $task,
        ]);
    }

    /**
     * Updates an existing Content model.
     *
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $content = $this->findModel($id, Article::className());
        $task    = $content->task;

        if (!isset($content, $task)) {
            throw new NotFoundHttpException("The user was not found.");
        }

        $content->setTags();

        if ($content->load(Yii::$app->request->post()) && $task->load(Yii::$app->request->post())) {
            if ($task->validate() && $task->save() && $content->validate()) {
                $content->save(false);
                $content->saveTags();

                return $this->redirect(['view', 'id' => $id]);
            }
        }

        return $this->render('update', [
            'model'     => $content,
            'modelTask' => $task,
        ]);
    }

    /**
     * Deletes an existing Content model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, Article::className())->delete();

        return $this->redirect(['index']);
    }

    /**
     * Поиск для Select2
     *
     * @param null $q
     * @param null $id
     * @return array
     */
    public function actionSearchLinking($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '', 'link' => '']];
        if (!is_null($q)) {
            $query = Article::find()->joinWith(['seo']);
            $query->select('seo_data.link as id, name AS text')
                ->where(['ilike', 'name', $q])
                ->orWhere(['ilike', 'seo_data.meta_title', $q])
                ->orWhere(['ilike', 'seo_data.keyword', $q])
                ->orWhere(['ilike', 'seo_data.keywords', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Article::findOne($id)->name];
        }
        return $out;
    }
}
