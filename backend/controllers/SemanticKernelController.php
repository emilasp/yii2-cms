<?php

namespace emilasp\cms\backend\controllers;

use emilasp\cms\common\models\ContentTask;
use emilasp\cms\common\models\search\ContentTaskSearch;
use Yii;
use emilasp\cms\common\models\CmsSemanticKernel;
use emilasp\cms\common\models\search\CmsSemanticKernelSearch;
use emilasp\core\components\base\Controller;
use yii\filters\VerbFilter;
use emilasp\rights\filters\AccessControl;

/**
 * SemanticKernelController implements the CRUD actions for CmsSemanticKernel model.
 */
class SemanticKernelController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CmsSemanticKernel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new CmsSemanticKernelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CmsSemanticKernel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, CmsSemanticKernel::class),
        ]);
    }

    /**
     * Creates a new CmsSemanticKernel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CmsSemanticKernel();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CmsSemanticKernel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, CmsSemanticKernel::class);

        $searchModelTasks  = new ContentTaskSearch(['semantic_kernel_id' => $model->id]);
        $dataProviderTasks = $searchModelTasks->search(Yii::$app->request->queryParams);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model'             => $model,
                'searchModelTasks'  => $searchModelTasks,
                'dataProviderTasks' => $dataProviderTasks,
            ]);
        }
    }

    /**
     * Deletes an existing CmsSemanticKernel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, CmsSemanticKernel::class)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Create Task
     */
    public function actionCreateTask()
    {
        $kernelId = Yii::$app->request->post('kernelId');
        $title  = Yii::$app->request->post('title');
        $keyword  = Yii::$app->request->post('keyword');
        $keywords = Yii::$app->request->post('keywords');
        $url      = Yii::$app->request->post('url');
        $length   = Yii::$app->request->post('length');
        $mission  = Yii::$app->request->post('mission');

        if (!$keyword || !$keywords || !$mission) {
            return $this->setAjaxResponse(self::AJAX_STATUS_ERROR, 'Fill keywords and mission');
        }

        $task = new ContentTask([
            'semantic_kernel_id' => $kernelId,
            'keyword'            => $keyword,
            'keywords'           => $keywords,
            'url'                => $url,
            'length'             => $length,
            'mission'            => $title . '<br />' . $mission,
            'type'               => ContentTask::TYPE_KEYWORD,
            'type_content'       => ContentTask::CONTENT_TYPE_ARTICLE,
            'status'             => ContentTask::STATUS_NEW,
        ]);

        if ($task->save()) {
            return $this->setAjaxResponse(self::AJAX_STATUS_SUCCESS, 'Create');
        }

        return $this->setAjaxResponse(self::AJAX_STATUS_ERROR, current($task->getErrors()));
    }
}
