<?php

namespace emilasp\cms\backend\controllers;

use emilasp\core\components\base\Controller;
use yii\filters\AccessControl;

/**
 * DashboardController implements the CRUD actions for Content model.
 */
class DashboardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Dashboard
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
