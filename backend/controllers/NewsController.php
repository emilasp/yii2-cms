<?php

namespace emilasp\cms\backend\controllers;

use emilasp\cms\common\models\ContentTask;
use emilasp\cms\common\models\News;
use emilasp\cms\common\models\search\NewsSearch;
use emilasp\core\components\base\Controller;
use Yii;
use emilasp\cms\common\models\search\ContentSearch;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * NewsController implements the CRUD actions for Content model.
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Content models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Content model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, News::className()),
        ]);
    }

    /**
     * Creates a new Content model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $content = new News();
        $task    = new ContentTask();

        if ($content->load(Yii::$app->request->post()) && $task->load(Yii::$app->request->post())) {
            $isValid = $content->validate();
            $isValid = $task->validate() && $isValid;

            if ($isValid && $content->save(false)) {
                $content->saveTags();
                $task->type_content = ContentTask::CONTENT_TYPE_NEWS;
                $task->content_id   = $content->id;
                $task->save(false);
                return $this->redirect(['index', 'id' => $content->id]);
            }
        }

        return $this->render('create', [
            'model'     => $content,
            'modelTask' => $task,
        ]);
    }

    /**
     * Updates an existing Content model.
     *
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $content = $this->findModel($id, News::className());
        $task    = $content->task;

        if (!isset($content, $task)) {
            throw new NotFoundHttpException("The user was not found.");
        }

        $content->setTags();

        if ($content->load(Yii::$app->request->post()) && $task->load(Yii::$app->request->post())) {
            if ($task->validate() && $task->save() && $content->validate()) {
                $content->save(false);
                $content->saveTags();
                return $this->redirect(['view', 'id' => $id]);
            }
        }

        return $this->render('update', [
            'model'     => $content,
            'modelTask' => $task,
        ]);
    }

    /**
     * Deletes an existing Content model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, News::className())->delete();

        return $this->redirect(['index']);
    }
}
