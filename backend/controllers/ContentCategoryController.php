<?php

namespace emilasp\cms\backend\controllers;

use emilasp\core\components\base\Controller;
use emilasp\core\extensions\ImeraviEditor\actions\ImageManagerAction;
use emilasp\core\extensions\ImeraviEditor\actions\UploadAction;
use Yii;
use emilasp\cms\common\models\ContentCategory;
use emilasp\cms\common\models\search\ContentCategorySearch;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * ContentCategoryController implements the CRUD actions for ContentCategory model.
 */
class ContentCategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'image-upload' => [
                'class'    => UploadAction::class,
                'url'      => '/uploads/',
                'path'     => '@backend/web/uploads',
                'translit' => true,
            ],

            'images-get' => [
                'class'   => ImageManagerAction::class,
                'url'     => '/uploads/',
                'path'    => '@backend/web/uploads',
                'options' => ['only' => ['*.jpg', '*.jpeg', '*.png', '*.gif', '*.ico']],
            ],
        ];
    }


    /**
     * Lists all ContentCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContentCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ContentCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, ContentCategory::className()),
        ]);
    }

    /**
     * Creates a new ContentCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ContentCategory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ContentCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, ContentCategory::className());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ContentCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, ContentCategory::className())->delete();

        return $this->redirect(['index']);
    }
}
