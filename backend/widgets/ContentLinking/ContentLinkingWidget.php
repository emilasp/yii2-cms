<?php
namespace emilasp\cms\backend\widgets\ContentLinking;

use yii;
use yii\helpers\Url;
use yii\widgets\Pjax;


/**
 * Расширение для перелинковки контента
 *
 * Class ContentLinkingWidget
 * @package emilasp\cms\backend\widgets\ContentLinking
 */
class ContentLinkingWidget extends yii\base\Widget
{
    /**
     * RUN
     */
    public function run()
    {
        echo $this->render('content-linking');
    }
}
