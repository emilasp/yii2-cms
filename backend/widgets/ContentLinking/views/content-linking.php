<?php

use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;

?>


<div class="row">
    <div class="col-md-6">
        <?= Select2::widget([
            'name'          => 'search-linking',
            'pluginOptions' => [
                'allowClear'         => true,
                'minimumInputLength' => 3,
                'language'           => [
                    'errorLoading' => new JsExpression("function () { return 'Ожидайте результатов...'; }"),
                ],
                'ajax'               => [
                    'url'      => Url::toRoute(['/cms/article/search-linking']),
                    'dataType' => 'json',
                    'data'     => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
                'templateResult'     => new JsExpression('function(model) { return model.text; }'),
                'templateSelection'  => new JsExpression('function (model) { return model.text; }'),
            ],
            'pluginEvents'  => [
                'change' => 'function(event) { 
                    $(".result_link").html("/" + this.value + ".html");
                    window.getSelection().removeAllRanges();
                    var range = document.createRange();
                    range.selectNode(document.getElementsByClassName("result_link")[0]);
                    window.getSelection().addRange(range);
                    document.execCommand("copy");
                }',
            ],
            'options'       => [
                'placeholder' => 'Поиск статьи для перелинковки'
            ]
        ]) ?>
    </div>
    <div class="col-md-6">
        <b class="result_link">-</b>
    </div>
</div>