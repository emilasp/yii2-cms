<?php

namespace emilasp\cms\backend\validators;

use emilasp\cms\common\models\Article;
use emilasp\cms\common\models\ContentTask;
use Yii;
use yii\validators\Validator;

/**
 * Валидатор статуса контента по заданию
 *
 * Class ContentStatusValidator
 * @package emilasp\cms\backend\validators
 */
class ContentStatusValidator extends Validator
{
    public $message = 'Status cannot be set until zapolneny the following attributes: ';

    /**
     * @param Article $model
     * @param string  $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if ($model->status == Article::STATUS_PUBLISH) {
            $task = $model->task;

            $this->message = Yii::t('cms', $this->message);

            if (!$task->mission) {
                $model->addError($attribute, $this->message . $task->getAttributeLabel('mission'));
            }

            if (!$task->plan) {
                $model->addError($attribute, $this->message . $task->getAttributeLabel('plan'));
            }

            $check = true;
            foreach (json_decode($task->checklist, true) as $row) {
                if (!$row['enabled']) {
                    $check = false;
                    break;
                }
            }

            if (!$check) {
                $model->addError($attribute, $this->message . $task->getAttributeLabel('checklist'));
            }

            if (!$model->seoTitle) {
                $model->addError($attribute, $this->message . $model->getAttributeLabel('seoTitle'));
            }

            if (!in_array($task->type, [ContentTask::TYPE_TASK]) && !$model->seoKeyword) {
                $model->addError($attribute, $this->message . $model->getAttributeLabel('seoKeyword'));
            }

            if ($task->type === ContentTask::TYPE_KEYWORD && !$task->keyword) {
                $model->addError($attribute, $this->message . $task->getAttributeLabel('keyword'));
            }

            if ($task->type === ContentTask::TYPE_REWRITING && !$task->url) {
                $model->addError($attribute, $this->message . $task->getAttributeLabel('url'));
            }

            if ($model->hasErrors('status')) {
                $model->status = Article::STATUS_DRAFTED;
            }
        }
    }
}
