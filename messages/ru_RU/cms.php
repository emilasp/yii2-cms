<?php
return [
    'Content'      => 'Контент',
    'Type Content' => 'Тип контента',
    'Checklist'    => 'Чеклист',
    'To Content'   => 'Перейти к контенту',


    'Article'                => 'Статья',
    'Articles'               => 'Статьи',
    'Create Article'         => 'Добавить статью',
    'Add Article'            => 'Добавить статью',
    'Update Content Article' => 'Редактирование статьи',

    'News'                => 'Новости',
    'Create News'         => 'Создание новости',
    'Add News'            => 'Добавить новость',
    'Update Content News' => 'Редактирование новости',

    'Content Task'        => 'Задание',
    'Content Tasks'       => 'Задания',
    'Create Content Task' => 'Добавление задания',
    'Add Content Task'    => 'Добавить задание',
    'Update Content Task' => 'Редактирование задания',
    'Mission'             => 'Миссия',
    'Plan'                => 'План(Задание)',
    'Length'              => 'Длина текста',
    'Keyword'             => 'Основной ключ',
    'Keywords'            => 'Дополнительные ключевые слова',

    'Set category for content.' => 'Нужно выбрать категорию для контента',
    'Create Content'            => 'Создать контент',
];
