<?php

namespace emilasp\cms\console\commands;

use DateTime;
use emilasp\cms\common\models\Article;
use emilasp\core\commands\AbstractConsoleController;
use Yii;


/**
 *  Скрипт публикует статьи
 * Запуск по крону раз в час
 *
 * @package emilasp\cms\console\commands
 */
class PublicationController extends AbstractConsoleController
{
    /**
     * Content publication
     */
    public function actionIndex(): void
    {
        $this->display("-----Content publication-----", self::FONT_COLOR_YELLOW);

        $lastContent = Article::find()
            ->where(['status' => Article::STATUS_PUBLISH])
            ->where(['IS NOT', 'published_at', null])
            ->orderBy('published_at DESC')
            ->one();

        $lastDate = $lastContent ? $lastContent->published_at : date('Y-m-d H:i:s', strtotime('-1 year'));

        $this->display("Last article: {$lastDate}");

        $hours = $this->getHoursAfteLastDate($lastDate);

        $hoursPerArticle = Yii::$app->getModule('cms')->getSetting('hours_per_article');

        $this->display("Hours: {$hours}, hourse per article: {$hoursPerArticle}");

        if ($hours > $hoursPerArticle) {
            $this->publication();
        }
    }

    /**
     * Получаем количество часов прошедших полсе последней публикации
     *
     * @param string $lastDate
     * @return int
     */
    private function getHoursAfteLastDate(string $lastDate): int
    {
        $dateTime            = new DateTime();
        $dateTimeLastArticle = new DateTime($lastDate);

        $diff  = $dateTime->diff($dateTimeLastArticle);
        return $diff->h + ($diff->days * 24);
    }

    /**
     * Публикуем статью
     */
    private function publication(): void
    {
        $article = Article::find()
            ->where(['status' => Article::STATUS_PUBLISH_WAIT])
            ->orderBy('created_at ASC')
            ->one();

        if ($article) {
            $article->status = Article::STATUS_PUBLISH;

            if ($article->save()) {
                $this->display("Опубликована статья: {$article->name}", AbstractConsoleController::FONT_COLOR_GREEN);
            } else {
                $this->display("Не удалось опубликовать статью", AbstractConsoleController::FONT_COLOR_RED);
                var_dump($article->getErrors());
                $article->status = Article::STATUS_PUBLISH_ERROR;
                $article->save(false);
            }
        } else {
            $this->display("Отсутствуют статьи для публикации", AbstractConsoleController::FONT_COLOR_RED);
        }
    }
}
