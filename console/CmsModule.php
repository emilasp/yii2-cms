<?php
namespace emilasp\cms\console;

use emilasp\cms\common\CmsBaseModule;

/**
 * Class CmsModule
 * @package emilasp\cms\console
 */
class CmsModule extends CmsBaseModule
{
    public $controllerNamespace = 'emilasp\cms\console\commands';
}
