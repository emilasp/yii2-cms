<?php

use emilasp\variety\models\Variety;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m170811_112108_add_table_content_news extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('cms_content_news', [
            'id'         => $this->primaryKey(11),
            'name'       => $this->string(255)->notNull(),
            'content'    => $this->text()->comment('Содержание'),
            'views'      => $this->integer(11)->defaultValue(0),
            'rating'     => $this->integer(11)->defaultValue(0),
            'image_id'   => $this->integer(11),
            'status'     => $this->smallInteger(1)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_cms_content_news_created_by',
            'cms_content_news',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_cms_content_news_updated_by',
            'cms_content_news',
            'updated_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_cms_content_news_image_id',
            'cms_content_news',
            'image_id',
            'media_file',
            'id'
        );

        $this->createIndex('idx_cms_content_news_views', 'cms_content_news', 'views');
        $this->createIndex('idx_cms_content_news_rating', 'cms_content_news', 'rating');

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('cms_content_news');

        $this->afterMigrate();
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
