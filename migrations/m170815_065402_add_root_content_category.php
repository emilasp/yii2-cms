<?php

use emilasp\taxonomy\models\Category;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m170815_065402_add_root_content_category extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        /** @var  $category */
        $category = new Category([
            'code'       => 'content',
            'name'       => 'Каталог статей',
            'text'       => 'Каталог для статей',
            'short_text' => 'Каталог для статей',
            'type'       => Category::TYPE_CATEGORY,
            'status'     => Category::STATUS_ENABLED,
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $category->makeRoot();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->truncateTable('taxonomy_category');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
