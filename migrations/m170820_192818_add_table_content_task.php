<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m170820_192818_add_table_content_task extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('cms_content_task', [
            'id'                 => $this->primaryKey(11),
            'content_id'         => $this->integer(11),
            'semantic_kernel_id' => $this->integer(11),
            'type'               => $this->smallInteger(1)->notNull(),
            'type_content'       => $this->smallInteger(1)->notNull(),
            'url'                => $this->string(255),
            'keyword'            => $this->string(255),
            'keywords'           => $this->text(),
            'length'             => $this->integer(),
            'mission'            => $this->text()->notNull()->comment('Миссия(задание)'),
            'plan'               => $this->text()->comment('План'),
            'checklist'          => 'JSONB',
            'status'             => $this->smallInteger(1)->notNull(),
            'created_at'         => $this->dateTime(),
            'updated_at'         => $this->dateTime(),
            'created_by'         => $this->integer(11),
            'updated_by'         => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_cms_content_task_created_by',
            'cms_content_task',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_cms_content_task_updated_by',
            'cms_content_task',
            'updated_by',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_cms_semantic_kernel_id',
            'cms_content_task',
            'semantic_kernel_id',
            'cms_semantic_kernel',
            'id'
        );

        $this->addForeignKey(
            'fk_cms_content_task_content_id',
            'cms_content_task',
            'content_id',
            'cms_content_article',
            'id'
        );

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('cms_content_task');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
