<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
 * Class m170820_192817_add_semantic_kernel*/
class m170820_192817_add_semantic_kernel extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
     * UP
     */
    public function up()
    {
        $this->createTable('cms_semantic_kernel', [
            'id'          => $this->primaryKey(11),
            'name'        => $this->string(255)->notNull(),
            'description' => $this->text(),
            'status'      => $this->smallInteger(1)->notNull(),
            'created_at'  => $this->dateTime(),
            'updated_at'  => $this->dateTime(),
            'created_by'  => $this->integer(11),
            'updated_by'  => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_cms_semantic_kernel_created_by',
            'cms_semantic_kernel',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_cms_semantic_kernel_updated_by',
            'cms_semantic_kernel',
            'updated_by',
            'users_user',
            'id'
        );

        $this->afterMigrate();
    }

    /**
     * DOWN
     */
    public function down()
    {
        $this->dropColumn('cms_content_task', 'semantic_kernel_id');
        $this->dropTable('cms_semantic_kernel');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
