<?php
namespace emilasp\cms\common\helpers;

/**
 * Class ContentHelper
 */
class ContentHelper
{
    /**
     * Дата для контента, с ограничением по времени
     *
     * @param string $datetime
     * @param int    $maxDays
     * @return string
     */
    public static function getFormatDateByTimeRule(string $datetime, int $maxDays = 365):? string
    {
        $time = strtotime($datetime . "+{$maxDays} days");
        if ($time > time()) {
            return $datetime;
        }
        return null;
    }
}