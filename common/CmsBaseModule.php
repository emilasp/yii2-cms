<?php
namespace emilasp\cms\common;

use emilasp\core\CoreModule;
use emilasp\settings\behaviors\SettingsBehavior;
use emilasp\settings\models\Setting;
use yii\helpers\ArrayHelper;

/**
 * Class CmsBaseModule
 * @package emilasp\cms\common
 */
class CmsBaseModule extends CoreModule
{
    /**
     * @return array
     */
    function behaviors()
    {
        return ArrayHelper::merge([
            'setting' => [
                'class'    => SettingsBehavior::className(),
                'meta'     => [
                    'name' => 'CMS',
                    'type' => Setting::TYPE_MODULE,
                ],
                'settings' => [
                    [
                        'code'    => 'categoryRootId',
                        'name'    => 'Root категория статьи',
                        'default' => 1,
                    ],
                    [
                        'code'    => 'hours_per_article',
                        'name'    => 'Частота публикаций статей',
                        'description'    => 'Раз в x часов публикуем 1 статью',
                        'default' => 24,
                    ],

                    /*[
                        'code'        => 'allow_client_sale',
                        'name'        => 'Разрешить онлайн продажи.',
                        'description' => 'Разрешить онлайн продажи.',
                        'default'     => '',
                        'data'      => [
                            ''                          => 'По умолчанию',
                            Setting::DEFAULT_SELECT_NO  => 'Нет',
                            Setting::DEFAULT_SELECT_YES => 'Да',
                            3                           => 'Не знаю',
                        ],
                    ],*/
                ],
            ],
        ], parent::behaviors());
    }
}
