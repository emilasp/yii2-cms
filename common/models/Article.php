<?php

namespace emilasp\cms\common\models;

use emilasp\cms\backend\validators\ContentStatusValidator;
use emilasp\cms\frontend\widgets\ContentDocumentWidget\ContentDocumentWidget;
use emilasp\core\behaviors\relations\ManyManyAttributeModelBehavior;
use emilasp\core\components\base\ActiveRecord;
use emilasp\core\extensions\CkeEditor\behaviors\SaveTmpImagesBehavior;
use emilasp\media\behaviors\FileBehavior;
use emilasp\media\behaviors\FileSingleBehavior;
use emilasp\media\models\File;
use emilasp\seo\behaviors\SeoBehavior;
use emilasp\seo\behaviors\SeoLinkingBehavior;
use emilasp\seo\models\SeoData;
use emilasp\seo\validators\SeoLinkValidator;
use emilasp\shortcode\behaviors\ShortCodeBehavior;
use emilasp\social\common\behaviors\ViewsBehavior;
use emilasp\social\frontend\behaviors\CommentBehavior;
use emilasp\social\frontend\behaviors\RatingBehavior;
use emilasp\taxonomy\behaviors\TagBehavior;
use emilasp\taxonomy\models\Category;
use emilasp\taxonomy\models\CategoryLink;
use emilasp\taxonomy\models\Tag;
use emilasp\taxonomy\models\TagLink;
use emilasp\users\common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cms_content_article".
 *
 * @property int         $id
 * @property string      $name
 * @property string      $entry Вступление(лид абзац - зацепить)
 * @property string      $content Содержание
 * @property string      $conclusion Заключение(итоги/выводы)
 * @property int         $rating
 * @property int         $image_id
 * @property int         $is_original
 * @property int         $status
 * @property string      $created_at
 * @property string      $updated_at
 * @property string      $published_at
 * @property int         $created_by
 * @property int         $updated_by
 *
 * @property User        $createdBy
 * @property User        $updatedBy
 * @property SeoData     $seo
 * @property Category    $category
 * @property Tag[]       $tags
 * @property File[]      $files
 * @property File        $image
 * @property File        $mainImage
 * @property ContentTask $task
 */
class Article extends ActiveRecord
{
    public const STATUS_PUBLISH       = 1;
    public const STATUS_PREPARATION   = 4;
    public const STATUS_PUBLISH_WAIT  = 5;
    public const STATUS_PUBLISH_ERROR = 6;

    public static $statuses = [
        self::STATUS_DRAFTED       => 'Drafted',
        self::STATUS_PUBLISH       => 'Published',
        self::STATUS_PUBLISH_WAIT  => 'Wait publish',
        self::STATUS_PUBLISH_ERROR => 'Error publish',
        self::STATUS_PREPARATION   => 'Preparation',
        self::STATUS_DISABLED      => 'Disabled',
        self::STATUS_DELETED       => 'Deleted',
    ];

    public $imageUpload;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_content_article';
    }


    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class'      => ShortCodeBehavior::class,
                'attributes' => ['content'],
                'shortCodes' => [
                    'document'    => [
                        'class'   => ContentDocumentWidget::class,
                    ],
                ]
            ],
            [
                'class' => SeoBehavior::class,
                'route' => '/cms/content/view'
            ],
            [
                'class' => SeoLinkingBehavior::class,
            ],
            'comments'           => [
                'class' => CommentBehavior::class,
            ],
            'rating'             => [
                'class' => RatingBehavior::class,
            ],
            [
                'class'         => ManyManyAttributeModelBehavior::className(),
                'attribute'     => 'categoriesId',
                'linkedClass'   => CategoryLink::class,
                'taxonomyClass' => ContentCategory::class,
                'relationName'  => 'categories',
            ],
            'files'              => [
                'class'     => FileBehavior::class,
                'attribute' => 'images',
            ],
            'image'              => [
                'class'         => FileSingleBehavior::class,
                'attribute'     => 'image_id',
                'formAttribute' => 'imageUpload',
            ],
            'tags'               => [
                'class'     => TagBehavior::class,
                'modelName' => ContentTag::class
            ],
            'cke_tmp_image_save' => [
                'class'      => SaveTmpImagesBehavior::class,
                'attributes' => ['entry', 'content', 'conclusion'],
            ],
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::class,
                'value' => function () {
                    return !isset(Yii::$app->user) ? 1 : Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [
                ['entry', 'content', 'conclusion'],
                'required',
                'when'       => function ($model) {
                    return $model->status === self::STATUS_ENABLED;
                },
                'whenClient' => "function (attribute, value) {
                    return $('#article-status').val() == '1';
                }"
            ],
            [['status', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['status', 'image_id', 'created_by', 'updated_by', 'is_original'], 'integer'],
            [['entry', 'content', 'conclusion'], 'string'],
            [['created_at', 'updated_at', 'published_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['updated_by' => 'id']
            ],

            ['formTags', 'safe'],
            ['categoriesId', 'safe'],

            [['seoH1', 'seoLink', 'seoTitle', 'seoKeyword', 'seoKeywords', 'seoDescription'], 'string', 'max' => 200],

            [['rating'], 'integer'],

            ['categoriesId', 'checkCategory'],

            [['imageUpload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],

            [
                'is_original',
                function ($attribute, $params) {
                    if (in_array($this->status, [self::STATUS_PUBLISH, self::STATUS_PUBLISH_WAIT])) {
                        if (!$this->is_original) {
                            $this->addError($attribute, 'Before published article - add to webmaster original.');
                        }
                    }
                }
            ],

            ['status', ContentStatusValidator::class, 'on' => 'update'],
            ['seoLink', SeoLinkValidator::class]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => Yii::t('cms', 'ID'),
            'name'         => Yii::t('site', 'Name'),
            'entry'        => Yii::t('cms', 'Вступление(лид абзац - зацепить)'),
            'content'      => Yii::t('cms', 'Содержание'),
            'conclusion'   => Yii::t('cms', 'Заключение(итоги/выводы)'),
            'rating'       => Yii::t('social', 'Rating'),
            'image_id'     => Yii::t('media', 'Image'),
            'imageUpload'  => Yii::t('media', 'Image'),
            'is_original'  => Yii::t('site', 'Original in webmaster'),
            'status'       => Yii::t('site', 'Status'),
            'created_at'   => Yii::t('site', 'Created At'),
            'updated_at'   => Yii::t('site', 'Updated At'),
            'published_at' => Yii::t('site', 'Published At'),
            'created_by'   => Yii::t('site', 'Created By'),
            'updated_by'   => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * Валидатор на обязательность категории
     *
     * @param $attribute
     * @param $params
     */
    public function checkCategory($attribute, $params)
    {
        $categoryIds = json_decode($this->categoriesId, true);

        if (!$categoryIds) {
            $this->addError($attribute, Yii::t('cms', 'Set category for content.'));
        }
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSeo()
    {
        return $this->hasOne(SeoData::class, ['object_id' => 'id'])
            ->andOnCondition(['object' => self::class]);
    }

    /**
     * @return ActiveQuery
     */
    public function getTagLink()
    {
        return $this->hasMany(TagLink::class, ['object_id' => 'id'])
            ->andOnCondition(['object' => self::class]);
    }

    /**
     * @return ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(ContentTag::class, ['id' => 'tag_id'])
            ->via('tagLink')->with(['seo']);
    }

    /**
     * @return ActiveQuery
     */
    public function getContentHasCategory()
    {
        return $this->hasMany(CategoryLink::class, ['object_id' => 'id'])
            ->where(['object' => self::class]);
    }

    /**
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ContentCategory::class, ['id' => 'taxonomy_id'])
            ->via('contentHasCategory')->with(['seo']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(ContentCategory::class, ['id' => 'taxonomy_id'])
            ->via('contentHasCategory')->with(['seo']);
    }

    /**
     * @return ActiveQuery
     */
    /*public function getFiles()
    {
        return $this->hasMany(File::class, ['object_id' => 'id'])
            ->where(['object' => self::class]);
    }*/

    /**
     * @return ActiveQuery
     */
    /*public function getImage()
    {
        return $this->hasOne(File::class, ['id' => 'image_id']);
    }*/

    /**
     * @return ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(ContentTask::class, ['content_id' => 'id']);
    }

    /** =======================================================================================
     * EVENTS
     * ======================================================================================= */

    /**
     * По событию перед сохранением
     *
     * @param bool $insert
     * @return mixed
     */
    public function beforeSave($insert)
    {
        if ($this->status === self::STATUS_ENABLED) {
            if ($insert || (!$insert && $this->isAttributeChanged('status'))) {
                $this->published_at = date('Y-m-d H:i:s');
            }
        }

        return parent::beforeSave($insert);
    }
}
