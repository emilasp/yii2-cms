<?php
namespace emilasp\cms\common\models;

use emilasp\seo\behaviors\SeoBehavior;
use emilasp\seo\models\SeoData;
use emilasp\taxonomy\models\Tag;
use emilasp\users\common\models\User;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "taxonomy_tag".
 *
 * @property integer $id
 * @property string  $slug
 * @property string  $name
 * @property integer $frequency
 * @property integer $status
 * @property string  $created_at
 * @property string  $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User    $createdBy
 * @property User    $updatedBy
 */
class ContentTag extends Tag
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'seo'                => [
                    'class' => SeoBehavior::className(),
                    'route' => '/cms/content/tag'
                ],
            ]
        );
    }

    /**
     * @return ActiveQuery
     */
    public function getSeo()
    {
        $object = $this;
        return $this->hasOne(SeoData::className(), ['object_id' => 'id'])
            ->andWhere(['object' => $object::className()]);
    }
}
