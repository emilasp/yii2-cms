<?php

namespace emilasp\cms\common\models;

use emilasp\seo\behaviors\SeoBehavior;
use emilasp\seo\models\SeoData;
use emilasp\taxonomy\models\Category;
use emilasp\taxonomy\models\CategoryQuery;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "taxonomy_category".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $text
 * @property string  $short_text
 * @property integer $image_id
 * @property integer $type
 * @property integer $status
 * @property integer $tree
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property string  $created_at
 * @property string  $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class ContentCategory extends Category
{
    const ROOT_CATEGORY = 1;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'seo'                => [
                    'class' => SeoBehavior::className(),
                    'route' => '/cms/content/category'
                ],
                'nestedSetsBehavior' => [
                    'class'         => \creocoder\nestedsets\NestedSetsBehavior::className(),
                    'treeAttribute' => 'tree',
                ]
            ]
        );
    }


    /**
     * @return CategoryQuery
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    /**
     * Формируем массив из полной ветки категорий
     *
     * @param string $route
     * @return array
     */
    public function getPathItems(string $route): array
    {
        $paths = [];
        foreach ($this->parents()->with(['seo'])->all() as $parent) {
            if ($parent->depth > 0) {
                $paths[] = ['label' => $parent->name, 'url' => $parent->getUrl($route), 'depth' => $parent->depth];
            }
        }

        $paths[] = ['label' => $this->name, 'url' => $this->getUrl($route), 'depth' => $this->depth];

        return $paths;
    }

    /**
     * @return ActiveQuery
     */
    public function getSeo()
    {
        $object = $this;
        return $this->hasOne(SeoData::className(), ['object_id' => 'id'])
            ->andWhere(['object' => $object::className()]);
    }
}
