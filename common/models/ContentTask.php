<?php

namespace emilasp\cms\common\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\json\behaviors\JsonFieldBehavior;
use emilasp\users\common\models\User;
use PhpOffice\PhpSpreadsheet\Writer\Ods\Content;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cms_content_task".
 *
 * @property int     $id
 * @property int     $content_id
 * @property int     $semantic_kernel_id
 * @property int     $type
 * @property int     $type_content
 * @property string  $url
 * @property string  $keyword
 * @property string  $keywords
 * @property int     $length
 * @property string  $mission Миссия(задание)
 * @property string  $plan План
 * @property string  $checklist Чеклист(соответствие)
 * @property int     $status
 * @property string  $created_at
 * @property string  $updated_at
 * @property int     $created_by
 * @property int     $updated_by
 *
 * @property Content $content
 * @property User    $createdBy
 * @property User    $updatedBy
 */
class ContentTask extends ActiveRecord
{
    public const TYPE_TASK      = 1;
    public const TYPE_KEYWORD   = 2;
    public const TYPE_REWRITING = 3;

    const        DEFAULT_PLAN   = ' 
                                    <h2>Статья</h2>
                                    <p><strong>Проблема</strong>:<br /></p>
                                    <p><strong>Структура</strong>:<br /></p>
                                    
                                    <h2>Общее</h2>
                                    <ul>
                                    <li><b>Заголовки Hх</b>
                                     - Весь текст должен быть структурирован, разделен на разделы подзаголовками от H2 до H4.</li>
                                    <li><b>Ключи</b> - должны быть распределены по всему тексту более-менее равномерно.</li>
                                    <li><b>Главный ключ</b> - должен быть употреблен в первом абзаце и ДО
первого подзаголовка.</li>
                                    <li><b>Списки</b> - Текст должен содержать списки.</li>
                                    <li><b>Блоки внимания - </b>Текст должен содержать важные мысли, так называемые блоки внимания.</li>
                                    <li>Статья должна быть логичной и последовательной</li>
                                    <li>Ключи в позаголовках - rлючи нужно употреблять как в тексте так и в подзаголовках(не во всех, только там где
будет читабельно)</li>
</ul>
';

    public static $types = [
        self::TYPE_TASK      => 'task',
        self::TYPE_KEYWORD   => 'keyword',
        self::TYPE_REWRITING => 'rewriting',
    ];

    public const STATUS_DELETE = 0;
    public const STATUS_NEW    = 1;
    public const STATUS_INWORK = 2;

    public static $statuses = [
        self::STATUS_NEW    => 'new',
        self::STATUS_INWORK => 'in work',
        self::STATUS_DELETE => 'delete',
    ];

    public const CONTENT_TYPE_ARTICLE = 1;
    public const CONTENT_TYPE_NEWS    = 2;

    public static $contentTypes = [
        self::CONTENT_TYPE_ARTICLE => 'article',
        self::CONTENT_TYPE_NEWS    => 'news',
    ];


    /**
     * @return array
     */
    public function behaviors(): array
    {
        return ArrayHelper::merge([
            [
                'class'        => JsonFieldBehavior::className(),
                'attribute'    => 'checklist',
                'scheme'       => [
                    'enabled'     => ['label' => 'Enabled', 'rules' => []],
                    'name'        => ['label' => 'Name', 'rules' => [['string']]],
                    'description' => ['label' => 'Description', 'rules' => [['string']]],
                ],
                'defaultValue' => [
                    ['name' => 'План', 'description' => 'Написать план', 'enabled' => 0],
                    ['name' => 'Иллюстрации', 'description' => 'Загрузить изображения', 'enabled' => 0],
                    ['name' => 'Проверить орфографию', 'description' => '', 'enabled' => 0],
                ]
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'cms_content_task';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['type', 'mission', 'status'], 'required'],
            [['length'], 'default', 'value' => 5000],
            [
                [
                    'content_id',
                    'semantic_kernel_id',
                    'type',
                    'type_content',
                    'length',
                    'status',
                    'created_by',
                    'updated_by'
                ],
                'integer'
            ],
            [['keywords', 'mission', 'plan', 'checklist'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['keyword'], 'string', 'max' => 255],
            [['url'], 'url'],
            [
                ['content_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Article::className(),
                'targetAttribute' => ['content_id' => 'id']
            ],
            [
                ['semantic_kernel_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CmsSemanticKernel::className(),
                'targetAttribute' => ['semantic_kernel_id' => 'id']
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id'                 => Yii::t('site', 'ID'),
            'content_id'         => Yii::t('cms', 'Content'),
            'semantic_kernel_id' => Yii::t('cms', 'Semantic Kernel'),
            'type'               => Yii::t('site', 'Type'),
            'type_content'       => Yii::t('cms', 'Type Content'),
            'url'                => Yii::t('site', 'Url'),
            'keyword'            => Yii::t('cms', 'Keyword'),
            'keywords'           => Yii::t('cms', 'Keywords'),
            'length'             => Yii::t('cms', 'Length'),
            'mission'            => Yii::t('cms', 'Mission'),
            'plan'               => Yii::t('cms', 'Plan'),
            'checklist'          => Yii::t('cms', 'Checklist'),
            'status'             => Yii::t('site', 'Status'),
            'created_at'         => Yii::t('site', 'Created At'),
            'updated_at'         => Yii::t('site', 'Updated At'),
            'created_by'         => Yii::t('site', 'Created By'),
            'updated_by'         => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContent()
    {
        return $this->hasOne(Article::className(), ['id' => 'content_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSemanticKernel()
    {
        return $this->hasOne(CmsSemanticKernel::className(), ['id' => 'semantic_kernel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * Before save
     *
     * @param bool $insert
     *
     * @return bool
     */
    public function beforeSave($insert): bool
    {
        if (parent::beforeSave($insert)) {
            $this->createContent();
            return true;
        }
        return false;
    }

    /**
     * Создаём контент
     */
    private function createContent(): void
    {
        if (!$this->content_id) {
            $content = new Article([
                'type'   => $this->type_content,
                'status' => Article::STATUS_DRAFTED,
                'name'   => Yii::t('cms', 'Content Task'),
            ]);
            $content->save(false);
            $this->content_id = $content->id;
        }
    }
}
