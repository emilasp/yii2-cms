<?php

namespace emilasp\cms\common\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\users\common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cms_semantic_kernel".
 *
 * @property integer       $id
 * @property string        $name
 * @property string        $description
 * @property integer       $status
 * @property string        $created_at
 * @property string        $updated_at
 * @property integer       $created_by
 * @property integer       $updated_by
 *
 * @property ContentTask[] $cmsContentTasks
 * @property User          $createdBy
 * @property User          $updatedBy
 */
class CmsSemanticKernel extends ActiveRecord
{
    public const STATUS_NEW      = 1;
    public const STATUS_IN_JOB   = 2;
    public const STATUS_COMPLETE = 3;

    public static $statuses = [
        self::STATUS_NEW      => 'New',
        self::STATUS_IN_JOB   => 'In job',
        self::STATUS_COMPLETE => 'Complete',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_semantic_kernel';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    return !isset(Yii::$app->user) ? 1 : Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['description'], 'string'],
            [['status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('cms', 'ID'),
            'name'        => Yii::t('cms', 'Name'),
            'description' => Yii::t('cms', 'Description'),
            'status'      => Yii::t('cms', 'Status'),
            'created_at'  => Yii::t('cms', 'Created At'),
            'updated_at'  => Yii::t('cms', 'Updated At'),
            'created_by'  => Yii::t('cms', 'Created By'),
            'updated_by'  => Yii::t('cms', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentTasks()
    {
        return $this->hasMany(ContentTask::className(), ['semantic_kernel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
