<?php

namespace emilasp\cms\common\models\search;

use emilasp\cms\common\models\ContentCategory;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\cms\common\models\ContentTask;
use yii\helpers\ArrayHelper;

/**
 * ContentTaskSearch represents the model behind the search form of `emilasp\cms\common\models\ContentTask`.
 */
class ContentTaskSearch extends ContentTask
{
    public $categories;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                [
                    'id',
                    'content_id',
                    'semantic_kernel_id',
                    'type',
                    'type_content',
                    'length',
                    'status',
                    'created_by',
                    'updated_by'
                ],
                'integer'
            ],
            [['url', 'keyword', 'keywords', 'mission', 'plan', 'created_at', 'updated_at', 'categories'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContentTask::find()->with('content');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'                 => $this->id,
            'content_id'         => $this->content_id,
            'semantic_kernel_id' => $this->semantic_kernel_id,
            'type'               => $this->type,
            'type_content'       => $this->type_content,
            'length'             => $this->length,
            'status'             => $this->status,
            'created_at'         => $this->created_at,
            'updated_at'         => $this->updated_at,
            'created_by'         => $this->created_by,
            'updated_by'         => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'url', $this->url])
            ->andFilterWhere(['ilike', 'keyword', $this->keyword])
            ->andFilterWhere(['ilike', 'keywords', $this->keywords])
            ->andFilterWhere(['ilike', 'mission', $this->mission])
            ->andFilterWhere(['ilike', 'plan', $this->plan]);


        if ($this->categories) {
            $categories = $this->categories;
            foreach ($this->categories as $categoryRoot) {
                $category   = ContentCategory::findOne($categoryRoot);
                $childs     = $category->leaves()->map()->all();
                $categories = ArrayHelper::merge($categories, array_keys($childs));
            }

            $query->joinWith('content.categories');

            $query->andFilterWhere(['taxonomy_link_category.taxonomy_id' => $categories]);
        }


        return $dataProvider;
    }
}
