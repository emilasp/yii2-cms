<?php

namespace emilasp\cms\common\models;

use emilasp\cms\backend\validators\ContentStatusValidator;
use emilasp\core\components\base\ActiveRecord;
use emilasp\core\extensions\CkeEditor\behaviors\SaveTmpImagesBehavior;
use emilasp\media\behaviors\FileBehavior;
use emilasp\media\behaviors\FileSingleBehavior;
use emilasp\media\models\File;
use emilasp\seo\behaviors\SeoBehavior;
use emilasp\seo\models\SeoData;
use emilasp\social\common\behaviors\ViewsBehavior;
use emilasp\social\frontend\behaviors\CommentBehavior;
use emilasp\social\frontend\behaviors\RatingBehavior;
use emilasp\taxonomy\behaviors\TagBehavior;
use emilasp\taxonomy\models\Category;
use emilasp\taxonomy\models\Tag;
use emilasp\taxonomy\models\TagLink;
use emilasp\users\common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cms_content_news".
 *
 * @property int         $id
 * @property string      $name
 * @property string      $content Содержание
 * @property int         $views
 * @property int         $rating
 * @property int         $image_id
 * @property int         $status
 * @property string      $created_at
 * @property string      $updated_at
 * @property int         $created_by
 * @property int         $updated_by
 *
 * @property User        $createdBy
 * @property User        $updatedBy
 * @property SeoData     $seo
 * @property Category    $category
 * @property Tag[]       $tags
 * @property File[]      $files
 * @property File        $image
 * @property ContentTask $task
 */
class News extends ActiveRecord
{
    public static $statuses = [
        self::STATUS_DRAFTED  => 'drafted',
        self::STATUS_ENABLED  => 'enabled',
        self::STATUS_DISABLED => 'disabled',
        self::STATUS_DELETED  => 'deleted',
    ];

    public $imageUpload;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_content_news';
    }


    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class' => SeoBehavior::className(),
                'route' => '/cms/article/view'
            ],
            /*'views'              => [
                'class' => ViewsBehavior::className(),
            ],*/
            'comments'           => [
                'class' => CommentBehavior::className(),
            ],
            'rating'             => [
                'class' => RatingBehavior::className(),
            ],
            'files'              => [
                'class'     => FileBehavior::className(),
                'attribute' => 'files',
            ],
            'image'              => [
                'class'         => FileSingleBehavior::className(),
                'attribute'     => 'image_id',
                'formAttribute' => 'imageUpload',
            ],
            'tags'               => [
                'class'     => TagBehavior::className(),
                'modelName' => ContentTag::className()
            ],
            'cke_tmp_image_save' => [
                'class'      => SaveTmpImagesBehavior::className(),
                'attributes' => ['content'],
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    return !isset(Yii::$app->user) ? 1 : Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [
                ['content'],
                'required',
                'when'       => function ($model) {
                    return $model->status === self::STATUS_ENABLED;
                },
                'whenClient' => "function (attribute, value) {
                    return $('#article-status').val() == '1';
                }"
            ],
            [['status', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['status', 'image_id', 'created_by', 'updated_by'], 'integer'],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],

            ['formTags', 'safe'],
            ['categoriesId', 'safe'],

            [['seoH1', 'seoLink', 'seoTitle', 'seoKeyword', 'seoKeywords', 'seoDescription'], 'string', 'max' => 200],

            [['views', 'rating'], 'integer'],

            [['imageUpload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],


            ['status', ContentStatusValidator::className(), 'on' => 'update']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('cms', 'ID'),
            'name'        => Yii::t('site', 'Name'),
            'content'     => Yii::t('cms', 'Содержание'),
            'views'       => Yii::t('social', 'Views'),
            'rating'      => Yii::t('social', 'Rating'),
            'image_id'    => Yii::t('media', 'Image'),
            'imageUpload' => Yii::t('media', 'Image'),
            'status'      => Yii::t('site', 'Status'),
            'created_at'  => Yii::t('site', 'Created At'),
            'updated_at'  => Yii::t('site', 'Updated At'),
            'created_by'  => Yii::t('site', 'Created By'),
            'updated_by'  => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSeo()
    {
        $object = $this;
        return $this->hasOne(SeoData::className(), ['object_id' => 'id'])
            ->andWhere(['object' => $object::className()]);
    }

    /**
     * @return ActiveQuery
     */
    public function getTagLink()
    {
        return $this->hasMany(TagLink::className(), ['object_id' => 'id'])
            ->andOnCondition(['object' => self::className()]);
    }

    /**
     * @return ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(ContentTag::className(), ['id' => 'tag_id'])
            ->via('tagLink');
    }

    /**
     * @return ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['object_id' => 'id'])
            ->where(['object' => self::className()]);
    }

    /**
     * @return ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(File::className(), ['id' => 'image_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(ContentTask::className(), ['content_id' => 'id']);
    }
}
