<?php

namespace emilasp\cms\frontend\components;

use emilasp\taxonomy\models\Category;
use Yii;
use yii\base\Component;
use yii\helpers\Url;

/**
 * Компонент формирует массив элементов меню
 *
 * Class CategoryMenuComponent
 * @package emilasp\cms\common\components
 */
class CategoryMenuComponent extends Component
{
    public $rootId = 1;

    /**
     * Формируем список категорий для меню
     *
     * @return array
     */
    public function getCategoryItems(): array
    {
        $this->rootId = Yii::$app->getModule('cms')->getSetting('categoryRootId');

        $items    = [];
        $category = Category::findOne($this->rootId);

        $leaves = $category->leaves()->all();

        foreach ($leaves as $item) {
            $items[] = [
                'label'  => $item->name,
                'url'    => Url::toRoute(['/cms/content/category', 'id' => $item->id]),
                'active' => false,
            ];
        }

        $items[] = [
            'label'  => Yii::t('taxonomy', 'All categories'),
            'url'    => Url::toRoute(['/cms/content/categories']),
            'active' => false,
        ];

        return $items;
    }
}
