<?php

namespace emilasp\cms\frontend\widgets\CategoryWidget;

use emilasp\core\components\base\AssetBundle;


/**
 * Class CategoryWidgetAsset
 * @package emilasp\cms\frontend\widgets\UserHistory
 */
class CategoryWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';
    public $css        = ['category-widget'];
}
