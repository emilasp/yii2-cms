<?php

use yii\helpers\Html;

?>


<?= Html::beginTag('ul', ['class' => 'css-treeview']) ?>

<?php foreach ($items as $index => $item) : ?>

    <?= $this->render('_item', ['item' => $item, 'index' => $index, 'isBase' => true]) ?>

<?php endforeach; ?>

<?= Html::endTag('ul') ?>
