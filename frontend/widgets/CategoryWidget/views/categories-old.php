<?php

use yii\helpers\Html;

?>

<div class="category-widget-container">

    <?= Html::beginTag('ul', ['class' => 'tree-container-ul']) ?>


    <?php foreach ($items as $item) : ?>

        <?= Html::beginTag('li') ?>


        <?= Html::a($item['name'], $item['url'], [
            'class' => 'sidebar-category ' . ($item['active'] ? 'category-active' : '')
        ]) ?>
        <?php if ($item['count']) : ?>
            <small>(<?= $item['count'] ?>)</small>
        <?php endif; ?>


        <?php if (!empty($item['items'])) : ?>

            <?= Html::beginTag('ul', ['class' => 'tree']) ?>

            <?php foreach ($item['items'] as $subItem) : ?>

                <?= Html::beginTag('li') ?>

                <?= Html::a($subItem['name'], $subItem['url'], [
                    'class' => 'sidebar-category ' . ($subItem['active'] ? 'category-active' : '')
                ]) ?>
                <?php if ($subItem['count']) : ?>
                    <small>(<?= $subItem['count'] ?>)</small>
                <?php endif; ?>

                <?= Html::endTag('li') ?>

            <?php endforeach; ?>

            <?= Html::endTag('ul') ?>

        <?php endif; ?>


        <?= Html::endTag('li') ?>

    <?php endforeach; ?>

    <?= Html::endTag('ul') ?>

</div>