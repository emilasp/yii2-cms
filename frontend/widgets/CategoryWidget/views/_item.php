<?php

use yii\helpers\Html;

?>

<?= Html::beginTag('li', ['class' => (!empty($isBase) ? 'asideBlockCatsHeader' : 'catChild')]) ?>

<?= Html::checkbox('check', true, ['id' => 'item-' . $index]) ?>

    <label for="item-<?= $index ?>"
           class="<?= !empty($item['items']) ? 'has-sub-categories' : '' ?>">

        <?= Html::a($item['name'], $item['url'], [
            'class' => 'sidebar-category ' . ($item['active'] ? 'category-active' : '')
        ]) ?>
        <?php if ($item['count']) : ?>
            <small>(<?= $item['count'] ?>)</small>
        <?php endif; ?>
    </label>

<?php if (!empty($item['items'])) : ?>

    <?= Html::beginTag('ul') ?>

    <?php foreach ($item['items'] as $indexSub => $subItem) : ?>

        <?= $this->render('_item', ['item' => $subItem, 'index' => $index . '-' . $indexSub]) ?>

    <?php endforeach; ?>

    <?= Html::endTag('ul') ?>

<?php endif; ?>


<?= Html::endTag('li') ?>