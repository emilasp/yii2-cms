<?php

namespace emilasp\cms\frontend\widgets\CategoryWidget;

use emilasp\cms\common\models\ContentCategory;
use emilasp\core\components\base\Widget;
use emilasp\taxonomy\models\Category;
use Yii;

/**
 * Расширение для вывода категорий в сайдбар
 *
 * Class CategoryWidget
 * @package emilasp\cms\frontend\widgets\CategoryWidget
 */
class CategoryWidget extends Widget
{
    public $type = 'article';

    /** @var  Category */
    public $categoryClass;

    /**
     * INIT
     */
    public function init()
    {
        $this->registerAssets();
    }

    /**
     * RUN
     */
    public function run()
    {
        echo $this->render('categories', [
            'items' => $this->getItems()
        ]);
    }

    /**
     * Получаем сортированный список категорий
     *
     * @return array|Category[]|\yii\db\ActiveRecord[]
     */
    private function getItems()
    {
        $key = 'category-widget:items';

        if (!$items = Yii::$app->cache->get($key)) {
            $items      = [];
            $categories = ContentCategory::find()
                ->where(['status' => Category::STATUS_ENABLED])
                ->andWhere(['depth' => 1])
                ->orderBy('lft')
                ->with(['seo'])
                ->all();

            foreach ($categories as $index => $category) {
                if ($itemRecursive = $this->getItemRecursive($category)) {
                    $items[] = $itemRecursive;
                }
            }

            Yii::$app->cache->set($key, $items, 10);
        }

        return $items;
    }

    /**
     * Рекурсивно формируем массив с категориями
     *
     * @param Category $category
     * @return array|null
     */
    private function getItemRecursive(Category $category):? array
    {
        $item = [
            'name'   => $category->name,
            'depth'  => $category->depth,
            'url'    => $category->getUrl('/cms/content/category'),
            'count'  => count($category->links),
            'active' => $this->isActiveItem($category),
        ];

        if ($childs = $category->children(1)->with(['seo'])->orderBy('name')->all()) {
            foreach ($childs as $indexSub => $child) {
                if ($itemRecursive = $this->getItemRecursive($child)) {
                    $item['items'][$indexSub] = $this->getItemRecursive($child);

                    $item['count'] += $item['items'][$indexSub]['count'];
                }
            }
        }

        return $item['count'] ? $item : null;
    }

    /**
     * Проверяем, что категория активна
     *
     * @param Category $category
     * @return bool
     */
    private function isActiveItem(
        Category $category
    ) {
        return Yii::$app->controller->id === $this->type
            && Yii::$app->controller->action->id === 'category'
            && $category->id == Yii::$app->request->get('id');
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        CategoryWidgetAsset::register($this->getView());
    }
}
