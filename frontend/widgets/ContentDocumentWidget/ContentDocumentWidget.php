<?php

namespace emilasp\cms\frontend\widgets\ContentDocumentWidget;

use emilasp\cms\common\models\Article;
use emilasp\core\components\base\ActiveRecord;
use emilasp\core\components\base\Widget;
use emilasp\media\models\File;
use emilasp\shortcode\interfaces\IShortCode;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * Виджет выводит теги
 *
 * Class ContentDocumentWidget
 * @package emilasp\cms\frontend\widgets\ContentDocumentWidget
 */
class ContentDocumentWidget extends Widget implements IShortCode
{
    /** @var int */
    public $modelId;

    public $name;

    /**
     * Init
     *
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        $this->registerAssets();
    }

    /**
     * Run
     */
    public function run()
    {
        echo $this->render('document-widget', [
            'document' => $this->getDocument()]
        );
    }

    /**
     * Get document
     *
     * @return File|null
     */
    private function getDocument():? File
    {
        return File::findOne($this->id);
    }

    /**
     * Registers the needed assets
     */
    private function registerAssets()
    {
        ContentDocumentWidgetAsset::register($this->view);
    }
}
