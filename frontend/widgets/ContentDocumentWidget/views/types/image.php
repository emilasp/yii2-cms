<?php

use emilasp\media\models\File;
use yii\helpers\Html;

?>

<?= Html::a(
    Html::img($file->getUrl(File::SIZE_MED), [
        'class'    => '',
        'width'    => '100%',
        'data-src' => $file->getUrl(File::SIZE_MAX),
        'alt'      => $file->title,
        'itemprop' => 'contentURL'
    ]),
    $file->getUrl(File::SIZE_MAX),
    ['data-jbox-image' => 'gl', 'data-pjax' => 0]
) ?>

