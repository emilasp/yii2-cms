<?php
use emilasp\media\models\File;
?>

<div class="card mb-3">
    <h5 class="card-header"><?= $document->title ?: 'Документ' ?></h5>
    <div class="card-body">
        <!--<h5 class="card-title">Специальный заголовок</h5>-->
        <div class="card-text">
            <?= $document->description ?>

            <?= $this->render('types/' . $document->type, ['file' => $document]) ?>
        </div>

        <div class="text-center mt-3">
            <a href="<?= $document->getUrl() ?>" class="btn btn-primary">
                <i class="fa fa-download"></i> Скачать
            </a>
        </div>

    </div>
</div>