<?php
namespace emilasp\cms\frontend\widgets\ContentDocumentWidget;

use emilasp\core\components\base\AssetBundle;

/**
 * Class ContentDocumentWidgetAsset
 * @package emilasp\cms\frontend\widgets\ContentDocumentWidget
 */
class ContentDocumentWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $css = ['document-widget'];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];
}
