<?php
namespace emilasp\cms\frontend\widgets\TagWidget;

use emilasp\core\components\base\AssetBundle;

/**
 * Class TagWidgetAsset
 * @package emilasp\cms\frontend\widgets\TagWidget
 */
class TagWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $css = ['tag-widget'];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];
}
