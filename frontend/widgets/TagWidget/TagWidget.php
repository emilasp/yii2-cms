<?php

namespace emilasp\cms\frontend\widgets\TagWidget;

use emilasp\cms\common\models\ContentTag;
use emilasp\core\components\base\Widget;
use emilasp\taxonomy\models\Tag;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


/**
 * Виджет выводит теги
 *
 * Class TagWidget
 * @package emilasp\cms\frontend\widgets\TagWidget
 */
class TagWidget extends Widget
{
    private const TAG_STYLES = [
        100 => [
            'font-size'  => '24px',
            'color' => 'green'
        ],
        80  => [
            'font-size'  => '22px',
            'color' => '#0a3e0a'
        ],
        60  => [
            'font-size'  => '20px',
            'color' => 'black'
        ],
        50  => [
            'font-size'  => '18px',
            'color' => '#313131'
        ],
        40  => [
            'font-size'  => '16px',
            'color' => '#525252'
        ],
        20  => [
            'font-size'  => '14px',
            'color' => '#808080'
        ],
        0   => [
            'font-size'  => '12px',
            'color' => '#b9b9b9'
        ],
    ];

    public $limit = 40;

    /**
     * Init
     *
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        $this->registerAssets();
    }

    /**
     * Run
     */
    public function run()
    {
        echo $this->render('tag-widget', ['items' => $this->getItems()]);
    }

    /**
     * Формируем массив тегов
     *
     * @return array
     */
    private function getItems(): array
    {

        $tags = ContentTag::find()->select('taxonomy_tag.*, count(taxonomy_tag.id) as frequency')->byStatus()
            ->with(['seo'])
            ->joinWith('objects')
            ->groupBy('taxonomy_tag.id')
            ->orderBy('count(taxonomy_tag.id) DESC')
            ->limit($this->limit)
            ->all();


        $counts = ArrayHelper::getColumn($tags, 'frequency');

        $max = $counts ? max($counts) : 0;
        $min = $counts ? min($counts) : 0;

        $tagsFormat = [];

        foreach ($tags as $tag) {
            $tagsFormat[] = [
                'tag'   => $tag,
                'style' => $this->getTagStyle($max, $min, $tag->frequency),
            ];
        }

        return $tagsFormat;
    }

    /**
     * Получаем стили для тега
     *
     * @param int $max
     * @param int $min
     * @param int $count
     * @return string
     */
    private function getTagStyle(int $max, int $min, int $count): string
    {
        $style    = self::TAG_STYLES[20];
        $percents = ($count - $min) / (($max - $min) ?: 1) * 100;

        foreach (self::TAG_STYLES as $precentSet => $setting) {
            if ($percents > $precentSet) {
                break;
            }
            $style = $setting;
        }
        return Html::cssStyleFromArray($style);
    }

    /**
     * Registers the needed assets
     */
    private function registerAssets()
    {
        TagWidgetAsset::register($this->view);
    }
}
