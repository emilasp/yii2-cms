<?php

?>

<div class="tags-widget">
    <?php foreach ($items as $item) : ?>
        <a class="tags-widget-tag" href="<?= $item['tag']->getUrl('/cms/content/tag') ?>" style="<?= $item['style'] ?>">
            <?= $item['tag']->name ?>
        </a>
    <?php endforeach; ?>
</div>