<?php

namespace emilasp\cms\frontend\widgets\ContentSidebarWidget;

use emilasp\cms\common\models\Article;
use emilasp\cms\common\models\Content;
use emilasp\core\components\base\Widget;
use Yii;

/**
 * Расширение для вывода категорий в сайдбар
 *
 * Class ContentSidebarWidget
 * @package emilasp\cms\frontend\widgets\ContentSidebarWidget
 */
class ContentSidebarWidget extends Widget
{
    const TYPE_POPULAR = 1;
    const TYPE_LATEST  = 2;

    /** @var  Content */
    public $className;
    public $type  = self::TYPE_POPULAR;
    public $limit = 5;

    private $items = [];

    /**
     * INIT
     */
    public function init()
    {
        $this->registerAssets();
    }

    /**
     * RUN
     */
    public function run()
    {
        echo $this->render('index', [
            'items' => $this->getItems(),
        ]);
    }

    /**
     * Получаем сортированный список категорий
     *
     * @return Content[]
     */
    private function getItems(): array
    {
        $items = [];
        switch ($this->type) {
            case self::TYPE_POPULAR:
                $items = Article::find()->joinWith(['seo'])
                    ->where(['cms_content_article.status' => Article::STATUS_PUBLISH])
                    ->orderBy('rating DESC, seo_data.views DESC')->limit($this->limit)->all();
                break;
            case self::TYPE_LATEST:
                $items = $this->className::find()->byStatus()->with(['seo'])
                    ->orderBy('created_at DESC')->limit($this->limit)->all();
                break;
        }
        return $items;
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        ContentSidebarWidgetAsset::register($this->getView());
    }
}
