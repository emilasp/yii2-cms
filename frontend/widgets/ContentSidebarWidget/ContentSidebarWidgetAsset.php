<?php

namespace emilasp\cms\frontend\widgets\ContentSidebarWidget;

use emilasp\core\components\base\AssetBundle;


/**
 * Class ContentSidebarWidgetAsset
 * @package emilasp\cms\frontend\widgets\ContentSidebarWidget
 */
class ContentSidebarWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';
    public $css        = ['content-sidebar-widget'];
}
