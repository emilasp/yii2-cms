<?php

use yii\helpers\Html;

?>

<ul>
    <?php foreach ($items as $index => $item) : ?>

        <?= $this->render('_item', ['item' => $item]) ?>

    <?php endforeach; ?>

</ul>