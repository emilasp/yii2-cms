<?php

use emilasp\core\helpers\StringHelper;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<li>

    <?= Html::a($item->name . ' ' . Html::tag('i', null, ['class' => 'fas fa-angle-double-right']), $item->getUrl()) ?>

    <div class="row text-meta">
        <div class="col-md-8">
            <span class="text-meta pr-1"><i class="fa fa-eye"></i> <?= $item->seo->getViewsCount() ?> </span>
            <span class="text-meta pr-1"><i class="fa fa-comments"></i> <?= $item->getCommentCount() ?> </span>
            <!--<time itemprop="datePublished" datetime="<?/*= $item->created_at */?>">
                <i class="fa fa-clock-o"></i>
                <?/*= Yii::$app->formatter->asDate($item->created_at) */?>
            </time>-->
        </div>
        <div class="col-md-4">
            <div class="float-md-right text-right">
                <span class="text-meta"><i class="fa fa-star"></i> <?= $item->getRating() ?: '-' ?> </span>

            </div>
        </div>
    </div>

    <!--<a itemprop="author" href="<?/*= Url::toRoute(['/users/profile/view', 'id' => $item->createdBy->id]) */?>">
        <i class="fa fa-user-circle-o pl-2"></i><?/*= $item->createdBy->profile->fullName */?>
    </a>-->
</li>