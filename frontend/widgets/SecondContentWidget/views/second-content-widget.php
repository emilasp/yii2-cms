<?php

use emilasp\media\models\File;
use yii\helpers\Html;

?>

<?php foreach ($posts as $model) : ?>

<div class="clearfix">
    <span itemscope itemtype="http://schema.org/ImageObject">
    <?php if ($model->mainImage) : ?>
        <?= Html::a(
            Html::img($model->mainImage->getUrl(File::SIZE_MID), [
                'class'    => 'mr-3 float-left section-image',
                'alt'      => $model->mainImage->title,
                'itemprop' => 'contentURL'
            ]),
            $model->getUrl(),
            ['data-pjax' => 0]
        ) ?>
        </span>
    <?php else : ?>
        <?= Html::img(File::getNoImageUrl(File::SIZE_MIN), ['class' => 'mr-3 float-left']); ?>
    <?php endif ?>


    <!--<div class="float-md-left text-muted">
                <i class="fa fa-clock-o"></i>
                <? /*= Yii::$app->formatter->asDate($model->created_at) */ ?>
                &nbsp;&nbsp;&nbsp;
            </div>
-->
    <h2>
        <a class="tag" href="<?= $model->getUrl() ?>" itemprop="relatedLink">
            <?= $model->name ?>
        </a>
    </h2>

    <?= $model->entry ?>

    <hr/>

</div>
<?php endforeach; ?>

