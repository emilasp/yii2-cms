<?php
namespace emilasp\cms\frontend\widgets\SecondContentWidget;

use emilasp\core\components\base\AssetBundle;

/**
 * Class SecondContentWidgetAsset
 * @package emilasp\cms\frontend\widgets\SecondContentWidget
 */
class SecondContentWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $css = ['second-content-widget'];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];
}
