<?php

namespace emilasp\cms\frontend\widgets\SecondContentWidget;

use emilasp\cms\common\models\Article;
use emilasp\core\components\base\ActiveRecord;
use emilasp\core\components\base\Widget;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Виджет выводит теги
 *
 * Class RelatedContentWidget
 * @package emilasp\cms\frontend\widgets\SecondContentWidget
 */
class SecondContentWidget extends Widget
{
    /** @var ActiveRecord */
    public $model;

    /** @var ActiveQuery */
    public $query;

    public $limit = 3;

    /**
     * Init
     *
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        $this->registerAssets();
    }

    /**
     * Run
     */
    public function run()
    {
        echo $this->render('second-content-widget', ['posts' => $this->getPosts()]);
    }

    /**
     * Получаем похожие записи
     *
     * @return array
     */
    private function getPosts(): array
    {
        return $this->query->limit($this->limit)->all();
    }

    /**
     * Registers the needed assets
     */
    private function registerAssets()
    {
        SecondContentWidgetAsset::register($this->view);
    }
}
