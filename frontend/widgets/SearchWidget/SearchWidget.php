<?php

namespace emilasp\cms\frontend\widgets\SearchWidget;

use emilasp\core\components\base\ActiveRecord;
use emilasp\core\components\base\Widget;
use emilasp\core\helpers\StringHelper;
use emilasp\media\models\File;
use Yii;
use yii\helpers\Html;

/**
 * Расширение для вывода категорий в сайдбар
 *
 * Class SearchWidget
 * @package emilasp\cms\frontend\widgets\SearchWidget
 */
class SearchWidget extends Widget
{
    /**
     * INIT
     */
    public function init()
    {
        parent::init();

        $this->registerJavascript();
    }
    /**
     * RUN
     */
    public function run()
    {
        $html = <<<HTML
 <div class="input-group content-search-widget">
    <input type="text" class="form-control content-search-input" placeholder="Поиск"
     aria-label="Input group example" aria-describedby="btnGroupAddon" value="{$this->getSearch()}" />
    <button type="button" class="btn btn-success content-search-button"><i class="fa fa-search"></i></button>
  </div>
HTML;


        echo $html;
    }

    /**
     * Получаем search term
     *
     * @return string
     */
    private function getSearch(): string
    {
        return Yii::$app->request->get('search', '');
    }

    /**
     * Register js
     */
    private function registerJavascript()
    {
        $js = <<<JS
        $(document).on('click', '.content-search-button', function () {
            redirect();
        });
        $('.content-search-input').keypress(function (e) {
          if (e.which == 13) {
            redirect();
            return false;
          }
        });
        
        function redirect() {
            var search = $('.content-search-input').val();
            
            if (search.length > 0) {
                location="/search/" + encodeURIComponent(search) + ".html";
            } else {
                notice('Строка поиска не должна быть пустой', 'red');
            }
        }

JS;

        $this->view->registerJs($js);
    }
}
