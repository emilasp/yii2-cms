<?php

namespace emilasp\cms\frontend\widgets\ShareSocialWidget;

use emilasp\core\components\base\AssetBundle;


/**
 * Class ShareSocialWidgetAsset
 * @package emilasp\cms\frontend\widgets\ShareSocialWidget
 */
class ShareSocialWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';
    public $css        = ['share-social-widget'];
}
