<?php

use yii\helpers\Html;

?>


<div class="social">
    <div class="social-title"><i class="fa fa-share"></i></div>
    <?php foreach ($items as $index => $item) : ?>
        <?= Html::a(
            '<i class="' . $item['iconClass'] . '"></i>',
            $item['url'] . '?' . http_build_query($item['options']),
            [
                'title' => 'Сохранить в ' . $item['name'],
                'class'  => 'share-social-widget-item ' . $index,
                'target' => '_blank',
                'onClick' => 'window.open(this.href, this.title, \'toolbar=0, status=0, width=548, height=325\'); return false'
            ]
        ) ?>
    <?php endforeach; ?>

</div>

