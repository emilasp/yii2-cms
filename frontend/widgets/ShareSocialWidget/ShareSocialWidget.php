<?php

namespace emilasp\cms\frontend\widgets\ShareSocialWidget;

use emilasp\core\components\base\ActiveRecord;
use emilasp\core\components\base\Widget;
use emilasp\core\helpers\StringHelper;
use emilasp\media\models\File;
use Yii;

/**
 * Расширение для вывода категорий в сайдбар
 *
 * Class ShareSocialWidget
 * @package emilasp\cms\frontend\widgets\ShareSocialWidget
 */
class ShareSocialWidget extends Widget
{
    //http://vkontakte.ru/share.php?url=[URL]&title=[TITLE]&description=[DESC]&image=[IMAGE]&noparse=true
    //http://connect.mail.ru/share?url=[URL]&title=[TITLE]&description=[DESC]&imageurl=[IMAGE]
    //http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=[URL]&st.comments=[TITLE]
    //http://connect.mail.ru/share?url=[URL]&title=[TITLE]&description=[DESC]&imageurl=[IMAGE]
    //https://www.facebook.com/sharer/sharer.php?u=[URL]
    //https://twitter.com/share?url=[URL]&text=[TITLE]
    //https://plus.google.com/share?url=[URL]
    //http://www.tumblr.com/share?v=3&u=[URL]&t=[TITLE]
    //http://pinterest.com/pin/create/button/?url=[URL]&media=[IMAGE]&description=[DESC]

    public $setMeta = true;

    private const SHARE_ELEMENTS = [
        'vk'            => [ /* https://vk.com/dev/widget_share */
            'name'    => 'Вконтакте',
            'url'     => 'http://vkontakte.ru/share.php',
            'options' => [
                'url'         => 'url',
                'title'       => 'title',
                'description' => 'description',
                'image'       => 'image',
                'noparse'     => true
            ]
        ],
        'odnoklassniki' => [ /* https://apiok.ru/ext/like */
            'name'    => 'Одноклассники',
            'url'     => 'https://connect.ok.ru/offer',
            'options' => [
                'url'         => 'url',
                'title'       => 'title',
                'description' => 'description',
                'imageUrl'    => 'image'
            ]
        ],
        'mailru'        => [ /* http://api.mail.ru/sites/plugins/share/extended/ */
            'name'      => 'mail.ru',
            'url'       => 'http://connect.mail.ru/share',
            'iconClass' => 'icon-mailru',
            'options'   => [
                'url'         => 'url',
                'title'       => 'title',
                'description' => 'description',
                'image_url'   => 'image',
            ]
        ],
        'facebook'      => [ /* https://habr.com/post/156185/ */
            'name'    => 'Facebook',
            'url'     => 'http://www.facebook.com/sharer.php',
            'options' => [
                's'            => '100',
                'p[url]'       => 'url',
                'p[title]'     => 'title',
                'p[summary]'   => 'description',
                'p[images][0]' => 'image',
            ]
        ],
        'twitter'       => [ /* https://developer.twitter.com/en/docs/twitter-for-websites/tweet-button/overview */
            'name'    => 'Twitter',
            'url'     => 'https://twitter.com/share',
            'options' => [
                'url'  => 'url',
                'text' => 'title',
            ]
        ],
        'google'        => [ /* https://developers.google.com/+/web/share/ */
            'name'    => 'Google+',
            'url'     => 'https://plus.google.com/share',
            'options' => [
                'url' => 'url',
            ]
        ],
        'tumblr'        => [ /*  */
            'name'    => 'mail.ru',
            'url'     => 'http://www.tumblr.com/share',
            'options' => [
                'u' => 'url',
                't' => 'title',
                'v' => '3',
            ]
        ],
        'pinterest'     => [ /* https://gist.github.com/chrisjlee/5196139 */
            'name'    => 'Pinterest',
            'url'     => 'http://pinterest.com/pin/create/button/',
            'options' => [
                'url'         => 'url',
                'description' => 'description',
                'media'       => 'image',
            ]
        ],
    ];

    /** @var  ActiveRecord */
    public $model;


    /**
     * INIT
     */
    public function init()
    {
        $this->registerAssets();
    }

    /**
     * RUN
     */
    public function run()
    {
        echo $this->render('share-social-widget', ['items' => $this->getItems()]);
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        ShareSocialWidgetAsset::register($this->getView());
    }

    /**
     * Заполняем настройки для ссылок
     *
     * @return array
     */
    private function getItems(): array
    {
        $items = self::SHARE_ELEMENTS;

        $params = [
            'locale'      => 'ru_RU',
            'site_name'   => Yii::$app->getModule('site')->getSetting('site_name'),
            'url'         => $this->model->getUrl(null, true),
            'title'       => strip_tags($this->model->name),
            'description' => strip_tags($this->model->entry),
        ];

        if ($imageUrl = $this->getImageUrl()) {
            $params['image'] = $imageUrl;
        }

        $this->setOggMeta($params);

        foreach ($items as $key => $setting) {
            foreach ($setting['options'] as $optionName => $option) {
                if (isset($params[$option])) {
                    $items[$key]['options'][$optionName] = $params[$option];
                }
            }

            $items[$key]['iconClass'] = $setting['iconClass'] ?? 'fab fa-' . $key;
        }

        return $items;
    }

    /**
     * Получаем изображение для контента
     *
     * @return string
     */
    private function getImageUrl(): ?string
    {
        $image = null;

        if ($this->model->mainImage) {
            $image = $this->model->mainImage->getUrl(File::SIZE_MID, false, true);
        }

        return $image;
    }

    /**
     * Добавляем Ogg META
     *
     * @param array $params
     */
    private function setOggMeta(array $params)
    {
        if ($this->setMeta) {
            foreach ($params as $name => $content) {
                if ($content) {
                    Yii::$app->controller->view->registerMetaTag(['property' => 'og:' . $name, 'content' => $content]);
                }
            }
        }
    }
}
