<?php
 use yii\helpers\Html;
 use emilasp\media\models\File;
?>


<div class="card-deck related-widget-block">

    <?php if ($items) : ?>
        <?php foreach ($items as $model) : ?>
            <div class="card related-widget-card">

                <?php if ($model->mainImage) : ?>
                <?= Html::a(
                    Html::img($model->mainImage->getUrl(File::SIZE_MID), [
                        'class'    => 'card-img-top section-image',
                        'alt'      => $model->mainImage->title,
                        'itemscope' => 'itemscope',
                        'itemtype' => 'http://schema.org/ImageObject',
                        'itemprop' => 'contentURL'
                    ]),
                    $model->getUrl(),
                    ['data-pjax' => 0]
                ) ?>
                <?php else : ?>
                    <?= Html::img(File::getNoImageUrl(File::SIZE_MIN), ['class' => 'card-img-top']); ?>
                <?php endif ?>


                <div class="card-body">
                    <div class="card-text">
                        <a class="tag" href="<?= $model->getUrl() ?>" itemprop="relatedLink">
                            <?= $model->name ?>
                        </a>
                    </div>
                    <p class="card-text text-right">
                        <small class="text-muted">
                            <i class="fa fa-clock-o"></i>
                            <?= Yii::$app->formatter->asDate($model->created_at) ?>
                        </small>
                    </p>
                </div>
            </div>


        <?php endforeach; ?>

    <?php else: ?>
        <p class="text-muted text-center">Подходящие материалы отсутствуют</p>
    <?php endif; ?>

</div>

