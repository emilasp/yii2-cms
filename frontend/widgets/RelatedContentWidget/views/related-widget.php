<?php

?>
<ul class="list-group">
    <?php if ($items) : ?>
        <?php foreach ($items as $item) : ?>
            <li class="list-group-item">
                <div class="float-md-left text-muted">
                    <i class="fa fa-clock-o"></i>
                    <?= Yii::$app->formatter->asDate($item->created_at) ?>
                    &nbsp;&nbsp;&nbsp;
                </div>

                <div class="float-md-left">
                    <a class="tag" href="<?= $item->getUrl() ?>" itemprop="relatedLink">
                        <?= $item->name ?>
                    </a>
                </div>
            </li>
        <?php endforeach; ?>
    <?php else: ?>
        <p class="text-muted">Подходящие материалы отсутствуют</p>
    <?php endif; ?>
</ul>

