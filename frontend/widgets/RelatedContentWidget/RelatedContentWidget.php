<?php

namespace emilasp\cms\frontend\widgets\RelatedContentWidget;

use emilasp\cms\common\models\Article;
use emilasp\core\components\base\ActiveRecord;
use emilasp\core\components\base\Widget;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * Виджет выводит теги
 *
 * Class RelatedContentWidget
 * @package emilasp\cms\frontend\widgets\RelatedContentWidget
 */
class RelatedContentWidget extends Widget
{
    /** @var ActiveRecord */
    public $model;

    public $limit = 3;

    /**
     * Init
     *
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        $this->registerAssets();
    }

    /**
     * Run
     */
    public function run()
    {
        echo $this->render('related-new-widget', ['items' => $this->getRelatedContent()]);
    }

    /**
     * Получаем похожие записи
     *
     * @return array
     */
    private function getRelatedContent(): array
    {
        if ($this->model) {
            $tags       = ArrayHelper::getColumn($this->model->tags, 'id');
            $categories = ArrayHelper::getColumn($this->model->categories, 'id');

            $models = Article::find()->byStatus()->joinWith('contentHasCategory')
                ->andWhere(['taxonomy_link_category.taxonomy_id' => $categories])
                ->andWhere(['<>', 'cms_content_article.id', $this->model->id])
                ->limit($this->limit)
                ->all();
        } else {
            $models = Article::find()->byStatus()->joinWith('contentHasCategory')
                ->limit($this->limit)
                ->all();
        }
        return $models;
    }

    /**
     * Поулчаем пересечения по тегам и
     *
     * @param array $categories
     * @param array $tags
     * @return array
     */
    private function findRelatedByCategoryAndTags(array $categories, array $tags): array
    {

    }


    private function findRelatedByTags(array $tags): array
    {

    }

    private function findRelatedByCategory(array $categories): array
    {

    }

    /**
     * Registers the needed assets
     */
    private function registerAssets()
    {
        RelatedContentWidgetAsset::register($this->view);
    }
}
