<?php
namespace emilasp\cms\frontend\widgets\RelatedContentWidget;

use emilasp\core\components\base\AssetBundle;

/**
 * Class RelatedContentWidgetAsset
 * @package emilasp\cms\frontend\widgets\RelatedContentWidget
 */
class RelatedContentWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $css = ['related-widget'];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];
}
