<?php

use emilasp\cms\common\helpers\ContentHelper;
use emilasp\cms\frontend\widgets\RelatedContentWidget\RelatedContentWidget;
use emilasp\cms\frontend\widgets\ShareSocialWidget\ShareSocialWidget;
use emilasp\media\models\File;
use emilasp\social\frontend\widgets\CommentWidget\CommentWidget;
use emilasp\social\frontend\widgets\RatingWidget\RatingWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model emilasp\cms\common\models\Article */

$this->title = $model->seo->meta_title;

$this->params['breadcrumbs']   = $model->category->getPathItems('/cms/content/category');
$this->params['breadcrumbs'][] = $this->title;
?>

<article itemscope itemtype="http://schema.org/Article">
    <header>

        <div class="page-head">
            <h1 itemprop="headline name"><?= $model->seo->h1 ?></h1>
            <div class="stripe-line"></div>
        </div>

        <meta itemscope itemprop="mainEntityOfPage" itemType="https://schema.org/WebPage"
              itemid="<?= $model->getUrl(null, true) ?>"/>


        <div class="meta">
            <div class="row">
                <div class="col-md-6 offset-md-6">
                    <?= ShareSocialWidget::widget(['model' => $model]) ?>
                </div>
            </div>

        </div>

        <div class="clearfix articleMarks">
            <div class="float-md-left">
                <div class="info-item" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                    <meta itemprop="name" content="<?= Yii::$app->getModule('site')->getSetting('site_name') ?>"/>
                    <meta itemprop="telephone" content="<?= Yii::$app->getModule('site')->getSetting('site_phone') ?>"/>
                    <meta itemprop="address" content="<?= Yii::$app->getModule('site')->getSetting('site_address') ?>"/>
                    <div class="meta-item" itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                        <img class="itemprops" itemprop="url image" src="<?= Url::to(['/'], true); ?>images/logo.png"
                             alt="<?= Yii::$app->getModule('site')->getSetting('site_name') ?>"/>
                    </div>
                </div>

                <?php if ($dateTime = ContentHelper::getFormatDateByTimeRule($model->updated_at)) : ?>
                <time datetime="<?= date('c', strtotime($dateTime)) ?>" itemprop="datePublished"
                      class="info-item">
                    <i class="fa fa-clock text-info"></i> <?= Yii::$app->formatter->asDate($dateTime) ?>
                </time>
                <?php endif; ?>

                <div class="info-item" itemprop="author" itemscope itemtype="http://schema.org/Person">
                    <i class="fa fa-user-circle text-info"></i>

                    <!-- <a href="<? /*= Url::toRoute([
                        '/users/profile/view',
                        'id' => $model->createdBy->id
                    ]) */ ?>">-->
                    <span itemprop="name" content="{{ theme.author }}">
                            <?= $model->createdBy->profile->fullName ?>
                        </span>
                    <!--</a>-->
                </div>

                <div class="info-item">
                    <i class="fa fa-eye text-info"></i>
                    <?= $model->seo->getViewsCount() ?>
                </div>

                <div class="info-item">
                    <i class="fas fa-comment text-info"></i> Комментарии:
                    <a href="#comment-tree-id"> <?= $model->getCommentCount() ?></a>
                </div>

                <?php if ($model->tags) : ?>
                    <div class="info-item">
                        <i class="fas fa-tags text-info"></i>Теги:
                        <div class="articleTagsList">
                            <?php foreach ($model->tags as $tag) : ?>
                                <?= Html::a($tag->name, $tag->getUrl()) ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endIf; ?>
            </div>
            <div class="float-md-right"><?= RatingWidget::widget(['model' => $model]) ?></div>
        </div>

    </header>


    <div itemprop="description" class="article-description clearfix">
        <!-- <?php /*if ($image = $model->mainImage): */ ?>
            <img itemprop="image" src="<? /*= $image->getUrl(File::SIZE_MID, false, true) */ ?>" class="image float-left"
                 alt="<? /*= $image->title */ ?>" />
        --><?php /*endIf; */ ?>

        <?= $model->entry ?>
    </div>

    <div itemprop="articleBody">
        <?= $model->content ?>

        <?= $model->conclusion ?>
    </div>

    <div class="footer clearfix">

        <div class="clearfix">
            <div class="float-md-left">
                <?php if ($model->tags) : ?>
                    <i class="fa fa-tags"></i>

                    <?php foreach ($model->tags as $tag) : ?>
                        <?= Html::a($tag->name, $tag->getUrl('/cms/article/tag'),
                            ['data-pjax' => 0, 'class' => 'tag']) ?>
                    <?php endforeach; ?>

                <?php endif; ?>
            </div>
        </div>

        <hr/>

        <div class="meta">
            <div class="row">
                <div class="col-md-6 offset-md-6">
                    <?= ShareSocialWidget::widget(['model' => $model, 'setMeta' => false]) ?>
                </div>
            </div>

        </div>

        <section class="content-block" itemscope itemtype="http://schema.org/WebPage">

            <div class="page-head">
                <h3 itemprop="headline name"><i class="fas fa-clone"></i> Похожие статьи</h3>
                <div class="stripe-line"></div>
            </div>


            <?= RelatedContentWidget::widget(['model' => $model]) ?>
        </section>

    </div>

</article>

<div class="content-block">

    <div class="page-head">
        <h3 itemprop="headline name"><i class="fas fa-clone"></i> Комментарии</h3>
        <div class="stripe-line"></div>
    </div>

    <?= CommentWidget::widget(['model' => $model]) ?>

</div>


<?php $this->registerJs('new jBox("Image")'); ?>
