<?php
/* @var $model emilasp\cms\common\models\Article */

use emilasp\media\models\File;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<article class="content-list">

    <h3>
        <?= Html::a($model->name, $model->getUrl(), [
            'class'     => '',
            'itemscope',
            'itemtype'  => 'https://schema.org/BlogPosting',
            'data-pjax' => 0
        ]) ?>
    </h3>

    <div>
        <span>
             <div class="badge badge badge-success list-post-rating">
                <i class="fa fa-star"></i> <?= $model->getRating() ?>
             </div>
        </span>

        <div>

            <div itemprop="about" class="clearfix">

                <span  itemscope itemtype="http://schema.org/ImageObject">
                <?php if ($model->mainImage) : ?>
                    <?= Html::a(
                        Html::img($model->mainImage->getUrl(File::SIZE_MID), [
                            'class'    => 'img-thumbnail float-left',
                            'data-src' => $model->mainImage->getUrl(File::SIZE_MAX),
                            'alt'      => $model->mainImage->title,
                            'itemprop' => 'contentURL'
                        ]),
                        $model->mainImage->getUrl(File::SIZE_MAX),
                        ['data-jbox-image' => 'gl', 'data-pjax' => 0]
                    ) ?>
                <?php else : ?>
                    <?= Html::img(File::getNoImageUrl(File::SIZE_MIN), ['class' => 'img-thumbnail float-left']); ?>
                <?php endif ?>
                </span>

                <?= $model->entry ?>
            </div>

        </div>

    </div>

    <div class="clearfix">

        <div class="float-md-left text-meta">

            <div class="clearfix">

                <div class="float-md-left text-meta">
                    <time itemprop="datePublished" datetime="<?= $model->created_at ?>">
                        <i class="fa fa-clock-o"></i>
                        <?= Yii::$app->formatter->asDate($model->created_at) ?>
                    </time>

                    <a itemprop="author" href="<?= Url::toRoute(['/users/profile/view', 'id' => $model->createdBy->id]) ?>">
                        <i class="fa fa-user-circle pl-2"></i>
                        <?= $model->createdBy->profile->fullName ?>
                    </a>
                </div>

                <div class="float-md-right text-right ml-5"> </span>
                    <span class="text-meta pr-1"><i class="fa fa-eye"></i> <?= $model->seo->getViewsCount() ?> </span>
                    <span class="text-meta pr-1"><i class="fa fa-comments"></i> <?= $model->getCommentCount() ?> </span>
                    <span class="text-meta"><i class="fa fa-star"></i> <?= $model->getRating() ?: '-' ?> </span>
                </div>

            </div>

            <!--<span itemprop="articleSection" class="mr-3">
                <i class="fa fa-folder-open text-meta ml-1"></i>

                <?php /*foreach ($model->category->getPathItems('/cms/content/category') as $parent) : */?>
                    <?/*= $parent['depth'] > 1 ? '<i class="fa fa-arrow-right"></i>' : '' */?>
                    <?/*= Html::a($parent['label'], $parent['url'], ['data-pjax' => 0]) */?>
                <?php /*endforeach; */?>
            </span>-->

            <?php if ($model->tags) : ?>
                <i class="fa fa-tags text-meta pl-3"></i>

                <?php foreach ($model->tags as $tag) : ?>
                    <span class="text-meta pr-1">
                    <?= Html::a('#' . $tag->name, $tag->getUrl('/cms/article/tag'), ['data-pjax' => 0]) ?>
                    </span>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>

        <div class="float-md-right text-right">
            <?= Html::a(Yii::t('cms', 'Link >>'), $model->getUrl(), [
                'class'     => 'btn btn-danger btn-sm',
                'data-pjax' => 0
            ]) ?>
        </div>
    </div>

</article>

