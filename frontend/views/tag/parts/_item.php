<?php
/* @var $model emilasp\cms\common\models\Article */

use emilasp\media\models\File;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<?= Html::beginTag('a', [
    'class'     => 'content-category-list-item',
    'href'      => $model->getUrl('/cms/content/category'),
    'data-pjax' => 0
]) ?>

<h4>
            <?= $model->name ?>
</h4>

<?= Html::endTag('a') ?>

