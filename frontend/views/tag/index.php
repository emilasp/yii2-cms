<?php

use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\cms\common\models\search\ContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = $this->title ?: Yii::t('taxonomy', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-index">

    <div class="page-head">
        <h1 itemprop="headline name"><?= $this->title ?></h1>
        <div class="stripe-line"></div>
    </div>

    <?php Pjax::begin(); ?>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView'     => 'parts/_item',
        'options'      => ['class' => 'row'],
        'itemOptions'  => ['class' => 'col-lg-4 col-md-3 col-sm-1'],
        'summary'      => '',
    ]); ?>

    <?php Pjax::end(); ?>

</div>

<?php $this->registerJs('new jBox("Image")'); ?>

