<?php

use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\cms\common\models\search\ContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = $model->seoTitle;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-index">

    <div class="page-head">
        <h1 itemprop="headline name"><?= $model->seoH1 ?></h1>
        <div class="stripe-line"></div>
    </div>

    <?php Pjax::begin(); ?>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView'     => '@vendor/emilasp/yii2-cms/frontend/views/article/parts/_item',
        'summary'      => '',
    ]); ?>

    <?php Pjax::end(); ?>
</div>

<?php $this->registerJs('new jBox("Image")'); ?>

