<?php
/* @var $model emilasp\cms\common\models\Article */

use emilasp\media\models\File;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<?= Html::beginTag('a', [
    'class'     => 'content-category-list-item',
    'href'      => $model->getUrl('/cms/content/category'),
    'data-pjax' => 0
]) ?>

<div itemprop="about" class="post-list-item clearfix">
        <span itemscope itemtype="http://schema.org/ImageObject">
        <?php if ($model->mainImage) : ?>
        <?= Html::img($model->mainImage->getUrl(File::SIZE_MID), [
            'class'    => 'img-thumbnail float-left',
            'data-src' => $model->mainImage->getUrl(File::SIZE_MAX),
            'alt'      => $model->mainImage->title,
            'itemprop' => 'contentURL'
        ]) ?>
        </span>
    <?php else : ?>
        <?= Html::img(File::getNoImageUrl(File::SIZE_MIN), ['class' => 'img-thumbnail float-left']); ?>
    <?php endif ?>

    <div class="post-list-item-description">
        <h4>
            <?= $model->name ?>
        </h4>
    </div>

</div>

<?= Html::endTag('a') ?>

