<?php

use emilasp\cms\frontend\widgets\SearchWidget\SearchWidget;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\cms\common\models\search\ContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = $this->title ?: Yii::t('cms', 'Search');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-index">

    <div class="page-head">
        <h1 itemprop="headline name"><?= $this->title ?></h1>
        <div class="stripe-line"></div>
    </div>


    <?php Pjax::begin(); ?>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView'     => function ($model, $key, $index, $widget) use ($searchData) {
            return $this->render('parts/_item', [
                'index'      => $index,
                'model'      => $model,
                'searchData' => $searchData,
            ]);
        },
        'summary'      => '',
    ]); ?>

    <?php Pjax::end(); ?>
</div>

<?php $this->registerJs('new jBox("Image")'); ?>

