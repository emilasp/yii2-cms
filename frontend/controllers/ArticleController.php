<?php

namespace emilasp\cms\frontend\controllers;

use emilasp\cms\common\models\Article;
use emilasp\cms\common\models\Content;
use emilasp\cms\common\models\ContentCategory;
use emilasp\cms\common\models\ContentTag;
use emilasp\core\components\base\Controller;
use emilasp\seo\behaviors\SeoMetaFilter;
use emilasp\taxonomy\models\Category;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * ArticleController
 */
class ArticleController extends Controller
{
    public static $modelClass = 'emilasp\cms\common\models\Article';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /*'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view'],
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow'   => true,
                        //'roles'   => ['*'],
                    ],
                ],
            ],*/
            'seo' => [
                'class'   => SeoMetaFilter::className(),
                'actions' => [
                    'view'     => ['className' => self::$modelClass, 'route' => '/cms/content/view'],
                ]
            ],
            [
                'class'      => 'yii\filters\PageCache',
                'only'       => ['view'],
                'duration'   => 0,
                'enabled'    => Yii::$app->user->isGuest && !YII_DEBUG,
                'dependency' => [
                    'class' => 'yii\caching\DbDependency',
                    'sql'   => 'SELECT CONCAT(MAX(social_comment.updated_at), MAX(cms_content_article.updated_at))
                                FROM social_comment, cms_content_article',
                ],
            ],
        ];
    }

    /**
     * Lists all Content models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Article::find()->byStatus(Article::STATUS_PUBLISH)->with(['categories', 'tags']),
            'sort'  => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     *  Displays a single Content model.
     *
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = Article::find()->where(['id' => $id])->with(['seo'])->one();
        $model->applyShortCodes();

        if ($model->status !== Article::STATUS_PUBLISH) {
            throw new HttpException(403, 'Access denny');
        }

        return $this->render('view', ['model' => $model]);
    }
}
