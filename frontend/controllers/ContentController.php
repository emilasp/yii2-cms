<?php

namespace emilasp\cms\frontend\controllers;

use emilasp\cms\common\models\Article;
use emilasp\cms\common\models\Content;
use emilasp\cms\common\models\ContentCategory;
use emilasp\cms\common\models\ContentTag;
use emilasp\cms\common\models\News;
use emilasp\core\components\base\Controller;
use emilasp\seo\behaviors\SeoMetaFilter;
use emilasp\seo\models\SeoData;
use emilasp\site\common\models\Page;
use emilasp\taxonomy\models\Category;
use emilasp\users\common\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * ArticleController
 */
class ContentController extends Controller
{
    public static $modelClass;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /*'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view'],
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow'   => true,
                        //'roles'   => ['*'],
                    ],
                ],
            ],*/
        ];
    }

    /**
     *  Displays a single Content model.
     *
     * @param string $title
     * @return string
     * @throws HttpException
     */
    public function actionView(string $title)
    {
        //Yii::$app->user->login(User::findOne(1), Yii::$app->getModule('users')->durationLogin);
        return $this->runAppAction($title);
    }

    /**
     *  Displays a category list contents
     *
     * @param string $title
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCategory(string $title)
    {
        return $this->runAppAction($title);
    }

    /**
     *  Displays a category list contents
     *
     * @param string $title
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionTag(string $title)
    {
        return $this->runAppAction($title);
    }

    /**
     * Run App action
     *
     * @param string $title
     * @return int|\yii\console\Response
     */
    private function runAppAction(string $title)
    {
        if (!$seo = SeoData::findOne(['link' => $title])) {
            return Yii::$app->runAction('/error');
        }

        $seo->processIncrementView();

        $route = $seo->route;

        switch ($seo->object) {
            case ContentCategory::class:
                $route = '/cms/category/view';
                break;
            case ContentTag::class:
                $route = '/cms/tag/view';
                break;
            case Article::class:
                $route = '/cms/article/view';
                break;

           /* case News::class:
                $route = '/cms/news/view';
                break;*/
        }
        return Yii::$app->runAction($route, ['id' => $seo->object_id]);
    }
}
