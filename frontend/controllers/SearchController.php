<?php

namespace emilasp\cms\frontend\controllers;

use emilasp\cms\common\models\Article;
use emilasp\cms\common\models\Content;
use emilasp\cms\common\models\ContentCategory;
use emilasp\cms\common\models\ContentTag;
use emilasp\core\components\base\Controller;
use emilasp\seo\behaviors\SeoMetaFilter;
use emilasp\taxonomy\models\Category;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * SearchController
 */
class SearchController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /*'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view'],
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow'   => true,
                        //'roles'   => ['*'],
                    ],
                ],
            ],*/
        ];
    }

    /**
     * Lists all Content models.
     * @return mixed
     */
    public function actionIndex(string $search)
    {
        $sql = <<<SQL
      SELECT id, ts_headline('russian', textContent, q) as content
        FROM cms_content_article,
        plainto_tsquery('russian','{$search}') as q,
        concat(entry, ' ',content, ' ',conclusion) as textContent
        WHERE to_tsvector('russian', textContent) @@ q;
SQL;

        $searchData = ArrayHelper::index(Yii::$app->db->createCommand($sql)->queryAll(), 'id');

        $dataProvider = new ActiveDataProvider([
            'query' => Article::find()->byStatus(Article::STATUS_PUBLISH)->with(['categories', 'tags'])
                ->andWhere(['id' => array_keys($searchData)]),
            'sort'  => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);
        return $this->render('index', [
            'searchData'   => $searchData,
            'dataProvider' => $dataProvider,
        ]);
    }
}
