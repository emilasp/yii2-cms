<?php

namespace emilasp\cms\frontend\controllers;

use emilasp\cms\common\models\Article;
use emilasp\cms\common\models\Content;
use emilasp\cms\common\models\ContentCategory;
use emilasp\cms\common\models\ContentTag;
use emilasp\core\components\base\Controller;
use emilasp\seo\behaviors\SeoMetaFilter;
use emilasp\taxonomy\models\Category;
use Yii;
use yii\caching\DbDependency;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * CategoryController
 */
class CategoryController extends Controller
{
    public static $modelClass = 'emilasp\cms\common\models\ContentCategory';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /*'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view'],
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow'   => true,
                        //'roles'   => ['*'],
                    ],
                ],
            ],*/
            'seo' => [
                'class'   => SeoMetaFilter::className(),
                'actions' => [
                    'view' => ['className' => self::$modelClass, 'route' => '/cms/content/category'],
                ]
            ],
            [
                'class'      => 'yii\filters\PageCache',
                'only'       => ['view'],
                'duration'   => 3600,
                'enabled'    => Yii::$app->user->isGuest && !YII_DEBUG,
                'dependency' => [
                    'class' => 'yii\caching\DbDependency',
                    'sql'   => 'SELECT CONCAT(MAX(taxonomy_category.updated_at), MAX(cms_content_article.updated_at))
                                FROM taxonomy_category, cms_content_article',
                ],
            ],
        ];
    }

    /**
     * Lists all Content models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ContentCategory::find()->byStatus(Article::STATUS_PUBLISH)->andWhere(['>', 'depth', 0]),
            'sort'  => ['defaultOrder' => ['id' => SORT_ASC]]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     *  Displays a single Content model.
     *
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function actionView($id)
    {
        if (!$category = ContentCategory::findOne($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->view->title = Yii::t('taxonomy', 'Category') . ': ' . $category->name;

        $categoryIds   = ArrayHelper::getColumn($category->children()->with(['seo'])->all(), 'id');
        $categoryIds[] = $id;

        $query = Article::find()
            ->with(['seo'])
            ->innerJoinWith('contentHasCategory')
            ->andWhere(['cms_content_article.status' => Article::STATUS_PUBLISH])
            ->andWhere(['taxonomy_link_category.taxonomy_id' => $categoryIds]);

        $dataProvider = new ActiveDataProvider(['query' => $query, 'sort' => ['defaultOrder' => ['id' => SORT_DESC]]]);

        return $this->render('view', [
            'model'        => $category,
            'dataProvider' => $dataProvider,
        ]);
    }
}
